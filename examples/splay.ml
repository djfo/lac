splay a t =
  match t with
    | nil         -> nil
    | (cl, c, cr) -> let x1 = a == c in
      if x1
      then (cl, c, cr)
      else let x2 = a < c in
        if x2
        then match cl with
          | nil         -> (nil, c, cr)
          | (bl, b, br) -> let x3 = a < b in
            if x3
            then
              let x4 = let x5 = nil in bl == x5 in
              if x4
              then (bl, b, (br, c, cr))
              else let x6 = nil in (* x6 = splay bl a *)
                match x6 with
                  | nil          -> nil
                  | (al, a', ar) ->
                    let x7 = (br, c, cr) in
                    let x8 = (ar, b, x7) in
                    (al, a', x8)
            else
              nil
        else
          nil;
