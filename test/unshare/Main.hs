{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.Expr.Typed         as Typed
import           Data.Expr.Typed.Unshare
import           Data.Type

import           Test.HUnit

main :: IO Counts
main = runTestTT tests
  where
    tests = TestList [
        testCase1
      , testCase2
      , testCase3
      ]

testCase1 = TestCase $ assertEqual "no trees #1" cmp1 (unshare cmp1)

testCase2 = TestCase $ assertEqual "no trees #2" cmp2 (unshare cmp2)

testCase3 = TestCase $ assertBool "trees #1" (ftt /= unshare ftt)

cmp1 = Typed.fromString "f x x <= f x y"

cmp2 = Typed.fromString "f x x <= f x (g y y)"

ftt = TyApp (TyApp (TyVar "f", tyHole) (TyVar "t", tyTree), tyHole) (TyVar "t", tyTree)

ftt_ftt = TyApp (ftt, tyHole) (ftt, tyHole)
