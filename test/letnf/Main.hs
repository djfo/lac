module Main where

import           Data.Expr

import           Test.HUnit

main :: IO Counts
main = runTestTT tests
  where
    tests = TestList [ letnf1
                     , letnf2
                     ]

letnf1 :: Test
letnf1 = TestCase $
  let e = Var "x" in
    assertEqual "letnf1" e (toLetNF e)

letnf2 :: Test
letnf2 = TestCase $
  let e = Lit LNil in
    assertEqual "letnf2" e (toLetNF e)
