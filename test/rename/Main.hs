{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.Expr.Typed as Typed

import           Test.HUnit

main :: IO Counts
main = runTestTT tests
  where
    tests = TestList [
        substLit
      , rename1
      , rename2
      , rename3
      , rename4
      ]

-- `substitute` test cases

substLit :: Test
substLit = TestCase $
  let e = Typed.fromString "x"
      t = Typed.fromString "y"
  in
  assertEqual "σ := {x → t}, σ̅(x) = t" (substitute "x" t e) t

-- `rename` test cases

rename1 :: Test
rename1 = TestCase $
  let expression = Typed.fromString "let x = z in x"
  in
  assertEqual "rename1"
    expression
    (rename "x" "x1" expression)

rename2 :: Test
rename2 = TestCase $
  assertEqual "rename2"
    (rename "z" "z1" $ Typed.fromString "let x = z in x")
    (Typed.fromString "let x = z1 in x")

rename3 :: Test
rename3 = TestCase $
  assertEqual "rename3"
    (rename "x" "x1" $ Typed.fromString "match x with | nil -> x | (x, y, z) -> x")
    (Typed.fromString "match x1 with | nil -> x1 | (x, y, z) -> x")

rename4 :: Test
rename4 = TestCase $
  assertEqual "rename4"
    (rename "a" "a1" $ Typed.fromString "match x with | nil -> x | (x, y, z) -> a")
    (Typed.fromString "match x with | nil -> x | (x, y, z) -> a1")
