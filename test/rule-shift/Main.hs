{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           Data.Expr.Typed          as Typed
import           Data.Type
import           Lac.Analysis.ProofTree
import           Lac.Analysis.Rules.Halt
import           Lac.Analysis.Rules.Shift
import           Lac.Analysis.Types
import           Lac.Analysis.Types.Ctx

import           Data.Default
import qualified Data.Map.Strict          as M
import           Data.Text                (Text)
import qualified Data.Vector              as V
import           Test.HUnit

main :: IO Counts
main = runTestTT tests
  where
    tests = TestList [
        shift1
      , shift2
      , shift3
      , shift4
      ]

shift1 = TestCase $ do
  let context =
        [ ("t", tyTree)
        , ("x", tyNat)
        ]
  x <- f
    (ruleShift ruleHalt ["t"])
    context
    (Typed.fromString "f x t", tyHole)
    (\ProofTree{..} -> let (_, _, Ctx{..}) = ptConclusion in ctxVariables)
    (const mempty)
  assertEqual "shift1" [("x", tyNat), ("t", tyTree)] x

shift2 = TestCase $ do
  let context =
        [ ("t1", tyTree)
        , ("x", tyNat)
        , ("t2", tyTree)
        , ("y", tyNat)
        ]
  x <- f
    (ruleShift ruleHalt ["t2", "t1"])
    context
    (Typed.fromString "f x t", tyHole)
    (\ProofTree{..} -> let (_, _, Ctx{..}) = ptConclusion in ctxVariables)
    (const mempty)
  assertEqual "shift2" [("x", tyNat), ("y", tyNat), ("t2", tyTree), ("t1", tyTree)] x

shift3 = TestCase $ do
  let context =
        [ ("t1", tyTree)
        , ("x", tyNat)
        , ("t2", tyTree)
        , ("y", tyNat)
        ]
  x <- f
    (ruleShift ruleHalt ["t1"])
    context
    (Typed.fromString "f x t", tyHole)
    (\ProofTree{..} -> let (_, _, Ctx{..}) = ptConclusion in ctxVariables)
    (const mempty)
  assertEqual "shift3" [("x", tyNat), ("y", tyNat), ("t2", tyTree), ("t1", tyTree)] x

shift4 = TestCase $
  do
    let context =
          [ ("t1", tyTree)
          , ("x", tyNat)
          , ("t2", tyTree)
          , ("y", tyNat)
          ]
    (before, after) <- f
      (ruleShift ruleHalt ["t1"])
      context
      (Typed.fromString "f x t", tyHole)
      (\ProofTree{..} -> let (q, _, q') = ptConclusion in (ctxCoefficients q, ctxCoefficients q'))
      (const mempty)
    assertEqual "shift4"
      (M.fromList . map (uncurry g) . M.toList $ before)
      after
  where
    g index coefficient =
      case index of
        RankIdx 1 -> (RankIdx 2, coefficient)
        RankIdx 2 -> (RankIdx 1, coefficient)
        VecIdx v ->
          let [v1, v2, v3] = V.toList v
          in
          (VecIdx $ V.fromList [v2, v1, v3], coefficient)

environment :: [a]
environment = mempty

f :: Monoid a => Rule -> [(Text, Type)] -> (Typed, Type) -> (ProofTree -> a) -> (Output -> a) -> IO a
f rule context typedExpression mapProofTree mapOutput = do
  (result, output) <- runGen environment $ do
    let bound = def
    q <- emptyCtx bound >>= flip (augmentCtx bound) context
    rule q typedExpression
  case result of
    Left theError   -> print theError >> error "error occured"
    Right theResult -> return $ mapProofTree theResult <> mapOutput output
