{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Expr.Typed as Typed
import           Data.Type

import           Data.List       (sort)
import           Test.HUnit

main :: IO Counts
main = runTestTT tests
  where
    tests = TestList [
        var1
      , var2
      , var3
      , var4
      , var5
      , var6
      , var7
      ]

var1 = TestCase $ assertEqual "var1" (free "x") ["x"]

var2 = TestCase $ assertEqual "var2" (free "f x") ["f", "x"]

var3 = TestCase $ assertEqual "var3" (free "nil") []

var4 = TestCase $ assertEqual "var4" (free "(nil, x, r)") (sort ["x", "r"])

var5 = TestCase $ assertEqual "var5" (free "if x then y else z") ["x", "y", "z"]

var6 = TestCase $ assertEqual "var6" (free "match t with | nil -> x | (l, y, r) -> y") (sort ["t", "x"])

var7 = TestCase $ assertEqual "var7" (free "match t with | nil -> x | (l, x, r) -> y") (sort ["t", "x", "y"])

free = map fst . sort . (\expression -> Typed.freeVariables (expression, tyHole)) . Typed.fromString
