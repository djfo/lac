{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.PP where

import           Data.Char
import           Data.Text (Text)
import qualified Data.Text as T

latexVar :: Text -> Text
latexVar x =
  let (p, s) = T.break isDigit x
      p'     = T.concatMap expand p
  in
  if T.all isDigit s
    then escape p' <> "_{" <> s <> "}"
    else escape p' <> s
  where
    expand = \case
      '_' -> "\\_"
      'л' -> "\\Lambda"
      x   -> T.singleton x

escape :: Text -> Text
escape = T.replace "$" "\\$"
