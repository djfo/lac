module Lac.Trace where

import Control.Applicative

traceShowM :: (Show a, Applicative f) => a -> f ()
traceShowM x = pure ()
