{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}

module Lac.Analysis.Graph (
    displayConstraintGraph
  ) where

import           Lac.Analysis.ProofTree
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint

import           Control.Monad.State.Strict.Ext
import           Control.Monad.Writer.Strict
import           Data.Text                      (Text)
import qualified Data.Text.Ext                  as T
import qualified Data.Text.IO                   as T
import qualified System.Info
import           System.Process

data DotInst
  = DotNode Text Text
  | DotEdge Text Text
  | DotArrow Text Text
  | DotBiArrow Text Text
  deriving (Eq, Show)

newtype MkGraph a
  = MkGraph {
    unMkGraph :: WriterT [DotInst] (State MkGraphState) a
  }
  deriving (
    Functor
  , Applicative
  , Monad
  , MonadState MkGraphState
  , MonadWriter [DotInst]
  )

data MkGraphState
  = MkGraphState {
    mgsFresh   :: Int
  , mgsMapping :: [(Coeff, Text)]
  }
  deriving (Eq, Ord, Show)

instance HasFresh MkGraphState where
  getFresh     = mgsFresh
  putFresh n s = s { mgsFresh = n }

makeDot :: (Coeff -> Text) -> Constraint Coeff -> MkGraph ()
makeDot coeffLookup (CRel op lhs rhs) =
  do
    x <- go lhs
    y <- go rhs
    case op of
      CEq -> tell [DotBiArrow x y]
      CGe -> tell [DotArrow x y]
      CLe -> tell [DotArrow y x]
  where
    go :: CExpr Coeff -> MkGraph Text
    go = \case
      CAtom c -> do
        identifier <- makeFreshIdentifierIfNeeded c
        tell [DotNode identifier (coeffLookup c)]
        return identifier
      CInt i -> do
        identifier <- freshIdentifier
        tell [DotNode identifier (T.show i)]
        return identifier
      CSum [expression] -> go expression
      CSum expressions -> do
        identifier <- freshIdentifier
        tell [DotNode identifier "Σ"]
        summands <- mapM go expressions
        forM_ summands $ \summand ->
          tell [DotEdge summand identifier]
        return identifier

makeFreshIdentifierIfNeeded :: Coeff -> MkGraph Text
makeFreshIdentifierIfNeeded coefficient = do
  mapping <- gets mgsMapping
  case lookup coefficient mapping of
    Just identifier -> return identifier
    Nothing         -> do
      identifier <- freshIdentifier
      s@MkGraphState{..} <- get
      put $ s { mgsMapping = (coefficient, identifier) : mgsMapping }
      return identifier

freshIdentifier :: MkGraph Text
freshIdentifier = (T.cons '_' . T.show) <$> fresh

toInternal :: Foldable t => (Coeff -> Text) -> t (Constraint Coeff) -> [DotInst]
toInternal coeffLookup constraints =
    (\((_, instructions), _) -> instructions)
  . flip runState (MkGraphState 0 mempty)
  . runWriterT
  . unMkGraph
  . mapM_ (makeDot coeffLookup)
  $ constraints

toDot :: [DotInst] -> Text
toDot instructions =
    T.intercalate "\n" $ concat [
        ["digraph {"]
      , map go instructions
      , ["}"]
      ]
  where
    go = \case
      DotEdge from to ->
        T.intercalate "\n" [ "subgraph {"
                           , "edge [dir=none];"
                           , from <> " -> " <> to
                           , "}"
                           ]
      DotBiArrow from to ->
        T.intercalate "\n" [ "subgraph {"
                           , "edge [dir=both];"
                           , from <> " -> " <> to
                           , "}"
                           ]
      DotArrow from to ->
        from <> " -> " <> to <> ";"
      DotNode identifier label ->
        identifier <> " [label=\"" <> escape label <> "\"];"

    escape = id

render :: (Coeff -> Text) -> [Constraint Coeff] -> Text
render f = toDot . toInternal f

displayConstraintGraph :: [ProofTree] -> IO ()
displayConstraintGraph proofTrees = do
  let coeffLookup = makeCoefficientLookup proofTrees
  let constraints = concatMap collectConstraints proofTrees
  let dotText = render coeffLookup constraints
  T.writeFile "constraints.dot" dotText
  callProcess "circo" ["-Tsvg", "-O", "constraints.dot"]
  case System.Info.os of
    "darwin"                 -> callProcess "open" ["-a", "Firefox.app", "constraints.dot.svg"]
    os | os `elem` ["linux"] -> callProcess "xdg-open" ["constraints.dot.svg"]
       | otherwise           -> putStrLn $ "unknown operating system: `" <> os <> "`"
