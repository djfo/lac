{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Solver (
    Result(..)
  , checkProofs
  ) where

import           Lac.Analysis.ProofTree
import           Lac.Analysis.Types            hiding (prove)
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint
import           Lac.Analysis.Types.Ctx.Ctx    (Ctx (..), coefficients)
import           Lac.Trace

import           Control.Arrow                 (first)
import           Control.Monad                 (foldM, forM_)
import           Data.Annotation
import           Data.Map.Strict               (Map)
import qualified Data.Map.Strict               as Map
import           Data.SBV
import qualified Data.Set                      as Set
import           Data.Text                     (Text)
import qualified Data.Text.Ext                 as Text
import qualified Data.Text.IO                  as Text
import qualified Data.Vector                   as Vector
import           Data.Word

data Result a b c
  = Sat a
  | Unsat b
  | DontKnow c

type Mapping = Map Coeff SDomain

-- Word8
-- type SDomain = SWord8
-- type Domain = Word8

-- Int8
-- type SDomain = SInt8
-- type Domain = Integer

-- Integer
type SDomain = SInteger
type Domain = Integer

checkProofs :: [(Text, ProofTree)] -> IO ()
checkProofs proofsForDefinitions =
  forM_ proofsForDefinitions $ \(definitionName, proofTree@ProofTree{..}) -> do
    let annotatedContexts = collectAnnotatedContexts proofTree
    let constraints = collectConstraints proofTree

    let (q, _, r) = ptConclusion

    traceShowM q

    let problem = do
          let constrainedAtoms = concatMap atoms constraints
          mapping <- makeMapping constrainedAtoms
          let symbolicConstraints = map (makeSymbConstr mapping) constraints
          forM_ symbolicConstraints $ \symbConstr ->
            constrain symbConstr

          let resultAtoms = coefficients q <> coefficients r
          let unconstrainedAtoms = Set.fromList resultAtoms `Set.difference` Set.fromList constrainedAtoms

          forM_ unconstrainedAtoms $ \atom -> do
            x <- free (show atom)
            constrain $ (x :: SDomain) .== 0

          -- TODO: ensure non-negative?

          -- let get coeff = Map.lookup coeff mapping
          -- let subjects = mapMaybe get . List.nub $ coefficients q <> coefficients r
          -- minimize "goal" $ sum subjects

    let config = z3 { transcript = Just "transcript.smt" }
    satResult@(SatResult smtResult) <- satWith config problem
    -- LexicographicResult smtResult <- optimize Lexicographic problem

    Text.putStrLn definitionName
    case smtResult of
      Unsatisfiable _ maybeUnsatCore ->
        putStrLn "Unsat"
      Satisfiable _ model -> do
        putStrLn "Sat"

        putStrLn "LHS"
        qa <- retrieveAnnotationForContext q smtResult
        Text.putStrLn . ppAnnotation $ qa

        putStrLn "RHS"
        ra <- retrieveAnnotationForContext r smtResult
        Text.putStrLn . ppAnnotation $ ra

      SatExtField _ _ ->
        putStrLn "SatExtField"
      Unknown _ _ ->
        putStrLn "Unknown"
      ProofError _ _ _ ->
        putStrLn "Error"

makeMapping :: [Coeff] -> Symbolic Mapping
makeMapping atoms = foldM
  (\m k -> do
    case Map.lookup k m of
      Just v -> return m
      Nothing -> do
        v <- free (show k)
        return $ Map.insert k v m)
  mempty
  atoms

makeSymbConstr :: Mapping -> Constraint Coeff -> SBool
makeSymbConstr mapping constraint =
  let rec = makeSymbConstr mapping in
  case constraint of
    CAnd constraints -> sAnd $ map rec constraints
    COr constraints  -> sOr $ map rec constraints
    CImp c d         -> rec c .=> rec d
    CRel op lhs rhs  -> expr lhs `f` expr rhs
      where
        f = case op of
          CEq -> (.==)
          CNe -> (./=)
          CLe -> (.<=)
          CGe -> (.>=)
  where
    expr :: CExpr Coeff -> SDomain
    expr = \case
      CAtom k       -> let Just x = Map.lookup k mapping in x
      CInt i        -> literal (fromIntegral i)
      CSum summands -> sum (map expr summands)
      CProd factors -> product (map expr factors)

retrieveAnnotationForContext :: (Monad m, Modelable a) => Ctx -> a -> m Annotation
retrieveAnnotationForContext Ctx{..} satResult = do
  let f acc (index, coeff) = do
        let key = show coeff
        case getModelValue key satResult :: Maybe Domain of
          Just value -> return $ (index, fromIntegral value) : acc
          Nothing    -> error "retrieveAnnotationForContext: no value"
  annotation <- foldM f [] (Map.toList ctxCoefficients)
  return $ makeAnnotation ("Q_{" <> Text.show ctxId <> "}") annotation

makeAnnotation :: Text -> [(Idx, Integer)] -> Annotation
makeAnnotation name = normalize . Annotation . map (first fromIntegral) . map translate
  where
    translate = \case
      (RankIdx x, c) -> (c, Rank (Text.show x))
      (VecIdx x⃗, c)  -> let terms = toLogTerms $ zip (map Text.show [1..]) (map fromIntegral . Vector.toList $ x⃗)
                        in
                        (c, Log terms)

    toLogTerms = \case
      []          -> []
      [(x, c)]    -> [(c, Cost)]
      (x, c) : xs -> (c, Size x) : toLogTerms xs
