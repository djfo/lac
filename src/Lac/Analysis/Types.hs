{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}

module Lac.Analysis.Types (
    Ctx()
  , latexCtx
  , emptyCtx
  , splitCtx
  , splitCtx'
  , ctxVars

  , coeff
  , coeffs

  , varIdx
  , varCoeff

  , freshCtx
  , augmentCtx
  , weakenCtx
  , returnCtx
  , forceCoeff
  , copyCtx
  , Idx(..)
  , astIdx
  , enumRankCoeffs
  , eqCtx
  , relCtx

  , Error(..)

  , Output(..)
  , outEq

  , CExpr(..)
  , mkEq
  , mkNe
  , mkLe
  , mkGe
  , mkAnd
  , mkOr
  , mkImp

  , Gen ()
  , runGen
  , GenConfig(..)
  , defGenConfig
  , tell
  , throwError
  , assert
  , assertWithContext
  , liftIO

  , getFunctionSymbols

  , Rule
  , setRuleName
  , costFree
  , isCostFree
  , accumConstr
  , prove
  , conclude

  , reset

  , lookupFunctionAnnotation
  , setFunctionAnnotation
  )
  where

import           Control.Monad.State.Strict.Ext
import           Data.Bound
import           Data.Expr.Typed
import           Data.Term
import           Data.Type
import           Lac.Analysis.ProofTree
import           Lac.Analysis.RuleName
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint
import           Lac.Analysis.Types.Ctx         as Ctx
import           Lac.Analysis.Types.Error
import           Lac.Trace
import           Latex

import           Control.Monad.Except
import           Control.Monad.Trans            (liftIO)
import           Control.Monad.Writer.Strict
import           Data.Default
import qualified Data.List.Ext                  as L
import           Data.Map.Strict.Ext            (Map)
import qualified Data.Map.Strict.Ext            as M
import qualified Data.Set                       as S
import           Data.Text                      (Text)
import qualified Data.Text.Ext                  as T
import qualified Data.Vector                    as V

-- * Basic types

freshCtx :: Gen Ctx
freshCtx = Ctx <$> fresh <*> pure mempty <*> pure mempty <*> pure mempty

emptyCtx :: Bound -> Gen Ctx
emptyCtx (Bound u) =
  do
    q <- freshCtx
    cs <- forM is $ \i -> fresh >>= \a -> return (i, Coeff a)
    return $ q { ctxCoefficients = M.fromList cs }
  where
    is = [VecIdx (V.singleton c) | c <- [0..u]]

augmentCtx :: Bound -> Ctx -> [Text] -> Gen Ctx
augmentCtx upperBound ctx@Ctx{..} xs =
  do
    let ts = ctxTrees ++ xs
    let nts = L.length ts

    traceShowM (ctxTrees, xs, show nts)

    rankCoefficients <- makeRankCoefficients nts
    vecCoefficients <- makeVectorCoefficients upperBound nts

    return $
      ctx { ctxTrees = ctxTrees ++ xs
          , ctxCoefficients =
              M.fromList rankCoefficients
                `M.union` M.fromList vecCoefficients
          }

splitCtx :: Bound -> Ctx -> [Text] -> Gen ([Text], Ctx)
splitCtx bound q xs = go q xs []
  where
    go :: Ctx -> [Text] -> [Text] -> Gen ([Text], Ctx)
    go ctx@Ctx{..} [] acc =
      do
        r <- emptyCtx bound
        r' <- augmentCtx bound r ctxTrees
        return (reverse acc, r')
    go ctx@Ctx{..} (y:ys) acc =
      if y `elem` ctxTrees then
          let ctx' = ctx { ctxTrees = L.delete y ctxTrees }
          in
          go ctx' ys (y : acc)
        else do
          ruleName <- getRuleName
          throwError . AssertionFailed $ "[" <> ruleName <> "] splitCtx: variable " <> y <> " not found in context"

splitCtx' :: Bound -> Ctx -> Text -> Gen (Text, Ctx)
splitCtx' u q y =
  splitCtx u q [y] >>=
    \(xs, q') ->
      case xs of
        [x] -> return (x, q')
        _   -> throwError . AssertionFailed $ "splitCtx': splitCtx returned /= 1 variable/type pair"

weakenCtx :: Bound -> Ctx -> [Text] -> Gen (Text, Ctx)
weakenCtx u q@Ctx{..} xs =
  case filter (`notElem` xs) ctxTrees of
    y : _ ->
      splitCtx u q [y] >>=
        \case
          ([y], q') -> return (y, q')
          _         -> throwError $ AssertionFailed "weakenCtx: unexpected splitCtx behavior"
    [] -> do
      ruleName <- getRuleName
      throwError . AssertionFailed $ "[" <> ruleName <> "] weakenCtx: cannot weaken"

coeff :: Ctx -> Idx -> Gen Coeff
coeff ctx idx =
  case coeff' ctx idx of
    Just c -> return c
    Nothing -> do
      ruleName <- getRuleName
      throwError . AssertionFailed $ "[" <> ruleName <> "] coefficient for index " <> T.show idx <> " not found"

coeff' :: Ctx -> Idx -> Maybe Coeff
coeff' Ctx{..} idx = M.lookup idx ctxCoefficients

coeffs :: Ctx -> [(Idx, Coeff)]
coeffs Ctx{..} = M.toList ctxCoefficients

varCoeff :: Ctx -> Text -> Gen Coeff
varCoeff q x = varIdx q x >>= coeff q

enumRankCoeffs :: Ctx -> [(Idx, Coeff)]
enumRankCoeffs Ctx{..} = filter (isRankIdx . fst) . M.toList $ ctxCoefficients

returnCtx :: Bound -> Gen Ctx
returnCtx b =
  emptyCtx b >>= \q ->
    augmentCtx b q ["*"]

copyCtx :: Ctx -> Gen Ctx
copyCtx q =
  do
    r <- freshCtx
    cs <- forM (M.toList . ctxCoefficients $ q) $
            \(i, _) ->
              Coeff <$> fresh >>= \c ->
                return (i, c)
    return $
      r { ctxTrees        = ctxTrees q
        , ctxCoefficients = M.fromList cs
        }

-- | Generate constraints to relate two annotated contexts with given relation.
relCtx :: (CExpr Coeff -> CExpr Coeff -> Constraint Coeff) -> Ctx -> Ctx -> Gen ()
relCtx mkRel q r =
  let f = S.fromList . M.keys . ctxCoefficients
      kq = f q
      kr = f r
  in
  if kq /= kr
    then do
      ruleName <- getRuleName
      let g = T.intercalate "," . map T.show . S.toList
      let m = "eqCtx (" <> ruleName <> "): contexts differ (Q: " <> g kq <> "; R: " <> g kr <> ")"
      throwError (AssertionFailed m)
    else
      forM_ kq $ \i -> do
        qi <- coeff q i
        ri <- coeff r i
        accumConstr $ [mkRel (CAtom qi) (CAtom ri)]

-- | Generate constraints to equate two annotated contexts.
eqCtx :: Ctx -> Ctx -> Gen ()
eqCtx = relCtx mkEq

instance Latex Ctx where
  latex Ctx{..} =
    if null ctxTrees
      then "\\varnothing"
      else T.intercalate ", " $ map (\x -> f (x, tyTree)) ctxTrees
    where
      f (x, ty) = x <> ": TODO"

-- * Execution

assert :: Bool -> Text -> Gen ()
assert p s
  | p         = return ()
  | otherwise = makeAssertionFailedError s >>= throwError

assertWithContext :: Ctx -> Typed -> Bool -> Text -> Gen ()
assertWithContext context expression assertion message
  | assertion = return ()
  | otherwise = failWithContext context expression message

failWithContext :: Ctx -> Typed -> Text -> Gen ()
failWithContext Ctx{..} expression message =
  makeAssertionFailedError message >>= \nested ->
    throwError $ RuleFailed ctxTrees expression nested

makeAssertionFailedError :: Text -> Gen Error
makeAssertionFailedError s = do
  ruleName <- getRuleName
  let s' = "[" <> ruleName <> "] " <> s
  return $ AssertionFailed s'

data Output
  = Output {
      outLog :: [Text]
    , outEqs :: [(Text, Text)]
  }
  deriving (Eq, Show)

outEq :: Text -> Text -> Output
outEq lhs rhs = Output mempty [(lhs, rhs)]

instance Semigroup Output where
  Output ys1 zs1 <> Output ys2 zs2 =
    Output (ys1 <> ys2) (zs1 <> zs2)

instance Monoid Output where
  mempty = Output mempty mempty

newtype Gen a = Gen {
    unGen :: ExceptT Error (StateT GenState (WriterT Output IO)) a
  }
  deriving (
    Functor
  , Applicative
  , Monad
  , MonadError Error
  , MonadState GenState
  , MonadWriter Output
  , MonadIO
  )

-- TODO: make `gsCostFree` read-only
data GenState
  = GenState {
    gsFresh                :: Int
  , gsProofTreeRuleName    :: Maybe RuleName
  , gsProofTreeConstraints :: [Constraint Coeff]
  , gsProofTreeSubtrees    :: [ProofTree]
  , gsCostFree             :: Bool
  , gsEnv                  :: Map Text TypedDecl
  , gsFunctionAnnotations  :: [(Text, (Ctx, Ctx))]
  }
  deriving Show

initState :: GenState
initState = GenState 0 Nothing mempty mempty False mempty mempty

type Rule = Ctx -> (Typed, Type) -> Gen ProofTree

setRuleName :: Text -> Gen ()
setRuleName n = do
  s@GenState{..} <- get
  if gsProofTreeRuleName /= Nothing
    then throwError $ AssertionFailed "rule name can only be set once"
    else
      let s' = s { gsProofTreeRuleName = Just (RuleName n) }
      in
      put s'

getRuleName :: Gen Text
getRuleName =
  gsProofTreeRuleName <$> get >>=
    return . maybe "<unset>" unRuleName

setCostFree :: Gen ()
setCostFree = modify $ \s -> s { gsCostFree = True }

isCostFree :: Gen Bool
isCostFree = gsCostFree <$> get

accumConstr :: [Constraint Coeff] -> Gen ()
accumConstr cs =
  modify $ \s@GenState{..} -> s { gsProofTreeConstraints = gsProofTreeConstraints ++ cs }

forceCoeff :: Idx -> Int -> Ctx -> Gen Ctx
forceCoeff index value annotatedContext@Ctx{..} = do
  qi <- coeff annotatedContext index
  return $ annotatedContext { ctxConstraints = mkEq (CAtom qi) (CInt value) : ctxConstraints }

reset :: Gen ()
reset = modify $ \state@GenState{..} ->
  initState
    { gsFresh = gsFresh
    , gsFunctionAnnotations = gsFunctionAnnotations
    }

prove :: Rule -> Ctx -> (Typed, Type) -> Gen Ctx
prove dispatch q typedExpression =
  do
    -- save current state
    saved <- get
    reset
    -- dispatch nested rule application
    t@ProofTree{..} <- dispatch q typedExpression
    -- recover state w/ updated "fresh" value
    modify $ \s -> saved { gsFresh             = gsFresh s
                         , gsProofTreeSubtrees = gsProofTreeSubtrees saved ++ [t]
                         }
    let (_, _, q') = ptConclusion
    return q'

costFree :: Rule -> Rule
costFree dispatch q e = setCostFree >> dispatch q e

conclude :: Ctx -> (Typed, Type) -> Ctx -> Gen ProofTree
conclude q tyExpr q' =
  do
    s@GenState{..} <- get
    case gsProofTreeRuleName of
      Just n -> do
        put $ s { gsProofTreeRuleName    = Nothing
                , gsProofTreeConstraints = []
                , gsProofTreeSubtrees    = []
                }
        return $
          ProofTree
            (q, tyExpr, q')
            n
            gsProofTreeConstraints
            gsProofTreeSubtrees
            Nothing
      Nothing ->
        throwError $ AssertionFailed "no rule name set"

lookupFunctionAnnotation :: Text -> Gen (Ctx, Ctx)
lookupFunctionAnnotation name = do
    lookup name <$> gets gsFunctionAnnotations >>= \case
      Just annotation -> return annotation
      Nothing         -> throwError $ AssertionFailed $ "no annotation found for function `" <> name <> "`"

setFunctionAnnotation :: Text -> (Ctx, Ctx) -> Gen ()
setFunctionAnnotation name annotatedContexts =
    modify $ \s -> s { gsFunctionAnnotations = (name, annotatedContexts) : gsFunctionAnnotations s }

instance HasFresh GenState where
  getFresh GenState{..} = gsFresh
  putFresh i s = s { gsFresh = i }

data GenConfig
  = GenConfig {
    gcMkDeclAnn :: TypedDecl -> Gen ()
  }

instance Show GenConfig where
  show GenConfig{..} = "GenConfig {..}"

defGenConfig :: GenConfig
defGenConfig =
  GenConfig {
    gcMkDeclAnn = defMkDeclAnn
  }

instance Default GenConfig where
  def = defGenConfig

defMkDeclAnn TypedDecl{..} = do
  let variables =
        case tyDeclType of
          F "->" [α, β] -> zip tyDeclArgs [α] -- ???
          -- TODO: fill in missing cases, or define function
  Ctx.fromType variables tyDeclType >>=
    setFunctionAnnotation tyDeclId

runGen :: GenConfig -> [TypedDecl] -> Gen r -> IO (Either Error r, Output)
runGen GenConfig{..} decls g = fmap f . runWriterT . flip runStateT s . runExceptT . unGen $ g'
  where
    f ((e, _), cs) = (e, cs)

    -- Set global function annotations before starting analysis.
    g' = mapM_ gcMkDeclAnn decls >> g

    s = initState { gsEnv = mkEnv decls }

    mkEnv = M.fromList . map (\tyDecl@TypedDecl{..} -> (tyDeclId, tyDecl))

-- * Interface

varIdx :: Ctx -> Text -> Gen Idx
varIdx q x =
  case L.elemIndex x . trees $ q of
    Just i  -> return . RankIdx . fromIntegral $ i + 1
    Nothing -> throwError $ AssertionFailed $ "varIdx: tree " <> x <> " not found in context"

getFunctionSymbols :: Gen [Text]
getFunctionSymbols = map fst <$> gets gsFunctionAnnotations
