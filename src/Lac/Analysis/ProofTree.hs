{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.ProofTree (
    ProofTree(..)

  , collectAnnotatedContexts
  , collectConstraints

  , latexProofTree

  , writeProofTrees
  , displayProofTrees
  , displayProofTree

  , makeCoefficientLookup
  ) where

import           Data.Expr.FromTyped
import           Data.Expr.Latex               ()
import           Data.Expr.Typed
import           Data.List.Ext                 (findWithIndex)
import           Data.Type                     (Type)
import           Lac.Analysis.RuleName
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint
import           Lac.Analysis.Types.Ctx
import           Latex

import           Data.Text                     (Text)
import qualified Data.Text.Ext                 as T
import qualified Data.Text.IO                  as T
import qualified Data.Vector                   as V
import qualified System.Info
import           System.Process

data ProofTree
  = ProofTree {
      ptConclusion  :: (Ctx, (Typed, Type), Ctx)
    , ptRuleName    :: RuleName
    , ptConstraints :: [Constraint Coeff]
    , ptSubtrees    :: [ProofTree]
    , ptComment     :: Maybe Text
    }
  deriving (Eq, Ord, Show)

collectAnnotatedContexts :: ProofTree -> [Ctx]
collectAnnotatedContexts ProofTree{..} =
    q : q' : concatMap collectAnnotatedContexts ptSubtrees
  where
    (q, _, q') = ptConclusion

collectConstraints :: ProofTree -> [Constraint Coeff]
collectConstraints ProofTree{..} = concat
    [ ctxConstraints inAnnotatedContext
    , ctxConstraints outAnnotatedContext
    , ptConstraints
    , concatMap collectConstraints ptSubtrees
    ]
  where
    (inAnnotatedContext, _, outAnnotatedContext) = ptConclusion

writeProofTrees :: FilePath -> [ProofTree] -> IO ()
writeProofTrees path proofTrees =
  do
    let render = latexProofTree (makeCoefficientLookup proofTrees)
    let rendered = map render proofTrees
    let text =
          "\\documentclass[10pt]{standalone}\
            \\\usepackage{amssymb}\
            \\\usepackage{proof}\
            \\\begin{document}\
            \\\def\arraystretch{1.4}"
          <> T.intercalate "\n\n" rendered
          <> "\\end{document}"
    T.writeFile path text

displayProofTrees :: [ProofTree] -> IO ()
displayProofTrees proofTrees =
  do
    writeProofTrees "temp.tex" proofTrees
    callProcess "pdflatex" ["temp"]
    case System.Info.os of
      "darwin"                       -> callProcess "open" ["-a", "Preview.app", "temp.pdf"]
      os       | os `elem` ["linux"] -> callProcess "xpdf" ["temp.pdf"]
               | otherwise           -> putStrLn $ "unknown operating system: `" <> os <> "`"

displayProofTree :: ProofTree -> IO ()
displayProofTree = displayProofTrees . (: [])

-- | Make lookup function from coefficients to human-readable identifiers
-- for a given list of proof trees.
makeCoefficientLookup :: [ProofTree] -> (Coeff -> Text)
makeCoefficientLookup proofTrees coefficient@(Coeff c) =
  let contexts = concatMap collectAnnotatedContexts proofTrees
  in
  case findWithIndex (lookupIndex coefficient) contexts of
    Just (elementIndex, coefficientIndex) ->
      let Ctx{..} = contexts !! elementIndex
          superscript = T.show ctxId
          subscript =
            case coefficientIndex of
              RankIdx i | costId `elem` ctxTrees -> "\\ast"
                        | otherwise              -> T.show i
              VecIdx v                           -> "(" <> T.intercalate ", " (map T.show . V.toList $ v) <> ")"
      in
      "q^{" <> superscript <> "}_{" <> subscript <> "}"
    Nothing ->
      "\\#_{" <> T.show c <> "}"

latexProofTree :: (Coeff -> Text) -> ProofTree -> Text
latexProofTree latexCoeff (ProofTree (q, (e, τ), r) (RuleName n) _cs ts mc) =
    "\\infer[(\\mathsf{" <> n
      <> "})]{" <> latexCtx q <> " \\vdash " <> latexTyped e <> " : " <> latexRetCtx (Just τ) r <> "}"
      <> "{"
      <> T.intercalate " & " (latexConstraints cs : map (latexProofTree latexCoeff) ts ++ latexComment)
      <> "}"
  where
    cs = ctxConstraints q <> _cs <> ctxConstraints r
    latexComment = maybe [] (:[]) mc
    latexTyped = latex . fromTyped
    latexConstraints = const $
        "\\begin{array}{l}"
          <> T.intercalate " \\\\\n" (map latexConstraint cs)
          <> "\\end{array}"
      where
        latexConstraint (CRel op e1 e2) = latexCExpr e1 <> " " <> latexOp op <> " " <> latexCExpr e2
        latexConstraint (CImp c1 c2) = "(" <> latexConstraint c1 <> " \\rightarrow " <> latexConstraint c2 <> ")"
        latexConstraint (CAnd cs) = T.intercalate " \\land " . map latexConstraint $ cs
        latexConstraint (COr cs) = T.intercalate " \\lor " . map latexConstraint $ cs

        latexOp = \case
          CEq -> "="
          CNe -> "\\neq"
          CLe -> "\\le"
          CGe -> "\\ge"

        latexCExpr (CAtom c) = latexCoeff c
        latexCExpr (CInt i)  = T.show i
        latexCExpr (CSum es) = T.intercalate " + " (map latexCExpr es)
