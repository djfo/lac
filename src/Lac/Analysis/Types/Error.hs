module Lac.Analysis.Types.Error where

import           Data.Expr.Typed
import           Data.Type

import           Data.Text       (Text)

data Error
  = NotImplemented Text
  | NotApplicable Text
  | AssertionFailed Text
  | RuleFailed [Text] Typed Error
  deriving Show
