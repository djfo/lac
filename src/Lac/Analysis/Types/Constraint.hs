{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Types.Constraint (
    Constraint(..)
  , CRelOp(..)
  , CExpr(..)
  , mkEq
  , mkNe
  , mkLe
  , mkGe
  , atoms
  , mkAnd
  , mkOr
  , mkImp
  , mapCoeff
  ) where

import           Lac.Analysis.Types.Coeff

import qualified Data.Text.Ext            as T

data Constraint a
  = CRel CRelOp (CExpr a) (CExpr a)
  | CAnd [Constraint a]
  | COr [Constraint a]
  | CImp (Constraint a) (Constraint a)
  deriving (Eq, Ord, Show)

data CRelOp
  = CEq -- ^ "equals"
  | CNe -- ^ "not equal to"
  | CLe -- ^ "less than or equal to"
  | CGe -- ^ "greater than or equal to"
  deriving (Eq, Ord, Show)

-- TODO: add `CNat` case? use `Word8`?
data CExpr a
  = CAtom a
  | CInt Int
  | CSum [CExpr a]
  | CProd [CExpr a]
  deriving (Eq, Ord, Show)

mkEq :: CExpr a -> CExpr a -> Constraint a
mkEq = CRel CEq

mkNe = CRel CNe

mkLe :: CExpr a -> CExpr a -> Constraint a
mkLe = CRel CLe

mkGe :: CExpr a -> CExpr a -> Constraint a
mkGe = CRel CGe

mkAnd = CAnd

mkOr = COr

mkImp = CImp

mapCoeff :: (a -> b) -> Constraint a -> Constraint b
mapCoeff f (CRel op e1 e2) =
    CRel op (go e1) (go e2)
  where
    go (CAtom x) = CAtom (f x)
    go (CInt c)  = CInt c
    go (CSum ts) = CSum $ map go ts

texCExpr (CAtom (Coeff i)) = "x" <> T.show i
texCExpr (CSum es) = "(+ " <> T.intercalate " " (map texCExpr es) <> ")"

atoms :: Constraint a -> [a]
atoms = \case
    CRel _ a b -> f a ++ f b
    CAnd cs    -> concatMap atoms cs
    COr cs     -> concatMap atoms cs
    CImp a b   -> concatMap atoms [a, b]
  where
    f (CAtom c)  = [c]
    f (CSum es)  = concatMap f es
    f (CProd es) = concatMap f es
    f (CInt _)   = []
