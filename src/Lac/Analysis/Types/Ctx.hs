{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Types.Ctx (
    module E
  , toAnnotation
  , makeRankCoefficients
  , makeVectorCoefficients
  , makeCoefficients
  , fromType
  ) where

import           Control.Monad.Except
import           Control.Monad.State.Strict.Ext
import           Data.Annotation
import           Data.Bound
import qualified Data.IntList                   as IntList
import           Data.List.Ext                  as List
import qualified Data.Map.Strict                as Map
import           Data.Term
import           Data.Type                      hiding (normalize)
import qualified Data.Vector                    as V
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Ctx.Ctx     as E
import           Lac.Analysis.Types.Ctx.PP      as E
import           Lac.Analysis.Types.Error

import           Data.Default
import           Data.Text                      (Text)
import qualified Data.Text.Ext                  as T
import           Data.Vector                    (Vector)
import qualified Data.Vector                    as Vector
import           Data.Word

toAnnotation :: Ctx -> (Idx -> Maybe Int) -> Annotation
toAnnotation annotatedContext@Ctx{..} assignment = normalize . Annotation $
    rankTerms ++ logTerms
  where
    rankTerms = map (uncurry rankTerm) . zip [1..] . trees $ annotatedContext

    logTerms = map (uncurry logTerm) . vectorIndices $ annotatedContext

    rankTerm :: Int -> Text -> (Int, Term)
    rankTerm i x =
      let Just c = assignment (RankIdx $ fromIntegral i)
      in
      (c, Rank x)

    logTerm :: Vector Word8 -> Coeff -> (Int, Term)
    logTerm v _ =
      let Just c   = assignment (VecIdx v)
          logTerms = map Size (trees annotatedContext) ++ [Cost]
          argument = flip zip logTerms . map fromIntegral . Vector.toList $ v
      in
      (fromIntegral c, Log argument)

makeRankCoefficients :: (MonadState s m, HasFresh s) => Int -> m [(Idx, Coeff)]
makeRankCoefficients size = let size' = fromIntegral size in
  do
    mapM
      (\x -> fresh >>= \i -> return (RankIdx x, Coeff i))
      [1..size']

makeVectorCoefficients :: (MonadState s m, HasFresh s) => Bound -> Int -> m [(Idx, Coeff)]
makeVectorCoefficients (Bound upperBound) size = let size' = fromIntegral size in
  mapM
    (\vec -> fresh >>= \i -> return (VecIdx (V.fromList vec), Coeff i))
    (IntList.enum upperBound (size' + 1))

makeCoefficients :: (MonadState s m, HasFresh s) => Bound -> Int -> m [(Idx, Coeff)]
makeCoefficients upperBound size =
  liftM2 (<>) (makeRankCoefficients size) (makeVectorCoefficients upperBound size)

fromType :: (MonadState s m, MonadError Error m, HasFresh s) => [(Text, Type)] -> Type -> m (Ctx, Ctx)
fromType variables = \case
    F "->" [argumentType,        returnType] -> go [argumentType] returnType
    F "->" [F "*" argumentTypes, returnType] -> go argumentTypes  returnType
    τ                                        -> throwError $ AssertionFailed $ "fromType: " <> T.show τ
  where
    go argumentTypes returnType = do
      q  <- make argumentTypes
      q' <- make [returnType]
      return (q, q')

    make types = do
      let trees = filter isTyTree types
      coefficients <- Map.fromList <$> makeCoefficients def (List.length trees)
      -- TODO: use helper function to construct context here
      Ctx <$> fresh <*> pure coefficients <*> pure (map fst . filter (isTyTree . snd) $ variables) <*> pure mempty
