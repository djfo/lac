{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Types.PP where

import           Data.Type
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint
import           Lac.Analysis.Types.Ctx

import qualified Data.Map                      as M
import           Data.Text.Ext                 (Text)
import qualified Data.Text.Ext                 as T
import qualified Data.Vector                   as V

ppConstr :: (a -> Text) -> Constraint a -> Text
ppConstr pp =
  \case
    CRel op lhs rhs -> ppCExpr lhs <> " " <> fmtOp op <> " " <> ppCExpr rhs
  where
    ppCExpr (CAtom x) = pp x
    ppCExpr (CSum es) = T.intercalate " + " (map ppCExpr es)
    ppCExpr (CInt c)  = T.show c

    fmtOp = \case
      CEq -> "="
      CLe -> "≤"
      CGe -> "≥"

ppCtx :: Ctx -> Text
ppCtx Ctx{..} =
  "variables: "
    <> T.intercalate ", " (map (\x -> ppVar (x, tyTree)) ctxTrees)
    <> "\n"
    <> "coefficients:\n"
    <> T.intercalate "\n" (map ppCoeff . M.toList $ ctxCoefficients)

ppVar :: (Text, Type) -> Text
ppVar (x, ty) = x <> " : " <> ppType ty

ppCoeff :: (Idx, Coeff) -> Text
ppCoeff (idx, (Coeff i)) = T.show i <> ": " <> c
  where
    c = case idx of
          RankIdx x  -> "q(" <> T.show x <> ")"
          -- TODO: fix output (?)
          VecIdx vec -> "q(" <> T.intercalate ", " (map T.show . V.toList $ vec) <> ")"
