{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Types.Ctx.Ctx where

import           Prelude                       hiding (length)

import           Data.Type
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint

import qualified Data.List                     as L
import           Data.Map.Strict               (Map)
import qualified Data.Map.Strict               as M
import           Data.Maybe                    (mapMaybe)
import           Data.Set                      (Set)
import qualified Data.Set                      as S
import           Data.Text                     (Text)
import           Data.Tuple                    (swap)
import           Data.Vector                   (Vector)
import qualified Data.Vector                   as V
import           Data.Word

{-# DEPRECATED nonTrees, nats "non-tree variables were removed from annotated contexts" #-}

data Ctx
  = Ctx {
    -- | Context identifier, i.e. 2 for context @Q_2@
    ctxId           :: Int
    -- | Coefficients in context
  , ctxCoefficients :: Map Idx Coeff
    -- | Variables in context
  , ctxTrees        :: [Text]
  , ctxConstraints  :: [Constraint Coeff]
  }
  deriving (Eq, Ord, Show)

length :: Ctx -> Int
length Ctx{..} = L.length ctxTrees

empty :: Ctx -> Bool
empty = (== 0) . length

ctxVars :: Ctx -> Set Text
ctxVars Ctx{..} = S.fromList ctxTrees

data Idx
  = RankIdx !Word8
  | VecIdx !(Vector Word8)
  deriving (Eq, Ord, Show)

mkRankIdx :: Integral a => a -> Idx
mkRankIdx = RankIdx . fromIntegral

mkVecIdx :: Integral a => [a] -> Idx
mkVecIdx = VecIdx . V.fromList . map fromIntegral

isRankIdx :: Idx -> Bool
isRankIdx =
  \case
    RankIdx _ -> True
    _         -> False

lookupIndex :: Coeff -> Ctx -> Maybe Idx
lookupIndex coeff Ctx{..} =
  L.lookup coeff . map swap . M.toList $ ctxCoefficients

astId :: Text
astId = "*"

astIdx :: Idx
astIdx = RankIdx 1

costId :: Text
costId = "+"

ptCoefficients :: Ctx -> [Coeff]
ptCoefficients Ctx{..} = map snd . M.toList $ ctxCoefficients

trees :: Ctx -> [Text]
trees Ctx{..} = ctxTrees

nats :: Ctx -> [Text]
nats Ctx{..} = []

nonTrees :: Ctx -> [(Text, Type)]
nonTrees = const []

vectorIndices :: Ctx -> [(Vector Word8, Coeff)]
vectorIndices Ctx{..} =
    mapMaybe (uncurry f) . M.toList $ ctxCoefficients
  where
    f (VecIdx v)  c = Just (v, c)
    f (RankIdx _) _ = Nothing

coefficients :: Ctx -> [Coeff]
coefficients Ctx{..} = map snd . M.toList $ ctxCoefficients
