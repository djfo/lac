{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Types.Ctx.PP where

import           Data.Type
import           Lac.Analysis.Types.Ctx.Ctx
import           Lac.PP

import           Data.Text                  (Text)
import qualified Data.Text.Ext              as T

latexCtx :: Ctx -> Text
latexCtx Ctx{..} = varCtx <> "|Q_{" <> T.show ctxId <> "}"
  where
    varCtx | null vars = "\\varnothing"
           | otherwise = T.intercalate ", " vars
    vars = Prelude.map var ctxTrees
    var x = latexVar x <> ": " <> latexType tyTree

latexRetCtx :: Maybe Type -> Ctx -> Text
latexRetCtx mTy Ctx{..} =
    texTy <> "|Q_{" <> T.show ctxId <> "}"
  where
    texTy =
      case mTy of
        Nothing -> "\\Box"
        Just ty -> latexType ty
