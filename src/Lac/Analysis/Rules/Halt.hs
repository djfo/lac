{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Halt where

import           Lac.Analysis.Rules.Common

ruleHalt :: Bool -> Rule
ruleHalt doesReturnTree annotatedContext typedExpression = do
  setRuleName "halt"
  q' <- emptyCtx def >>=
          if doesReturnTree
              then flip (augmentCtx def) ["*"]
              else return . id
  conclude annotatedContext typedExpression q'
