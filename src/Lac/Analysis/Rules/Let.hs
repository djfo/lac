{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Let where

import           Data.Expr.FromTyped
import           Data.Expr.Types               (var)
import qualified Data.IntList                  as IntList
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx        as Ctx

import qualified Data.List.Ext                 as L
import qualified Data.Set                      as S
import qualified Data.Vector                   as Vector
import           Data.Word

ruleLet
  :: Rule           -- ^ continuation
  -> Rule
  -> Rule
  -> Ctx            -- ^ context/annotation (i.e. Γ, Δ)
  -> (Typed, Type)  -- ^ expression
  -> Gen ProofTree
ruleLet rule1 rule2 rule3 q e@(TyLet x (e1, τx) (e2, τe2), τe) =
  do
    let u@(Bound ub) = def

    -- TODO: check variable ordering

    -- TODO: assert linearity

    functionSymbols <- S.fromList <$> getFunctionSymbols

    let var' = (S.\\ functionSymbols) . var . fromTyped

    let vΓΔ = ctxVars q   -- all variables in context Q
    let vΓ  = S.filter (`elem` trees q) . var' $ e1   -- variables in expression e1
    let vΔ0 = vΓΔ S.\\ vΓ -- remaining variables (i.e. variables in
                          -- expression e2 and variables that later will be
                          -- subject to weakening)
    let vΔ = vΔ0

    (_, p) <- splitCtx u q (S.toList vΔ0)
    (_, r) <- splitCtx u q (S.toList vΓ)
    r' <- augmentCtx u r [x]

    let m  = Ctx.length p
    let m' = fromIntegral m :: Word8
    let k  = Ctx.length r
    let k' = fromIntegral k :: Word8

    s <- prove rule1 p  (e1, τx)  -- P'
    t <- prove rule2 r' (e2, τe2) -- R'

    -- p_i = q_i
    forM_ [1..m'] $ \i -> do
      let idx = RankIdx i
      pi <- coeff p idx
      qi <- coeff q idx
      accumConstr [ mkEq (CAtom pi) (CAtom qi) ]

    -- p_{(\vec{a},c)} = q_{(\vec{a},\vec{0},c)}
    let allVecA = IntList.enum ub m'
    let vecΔ0 = replicate k 0
    forM_ allVecA $ \xs ->
      forM_ [0..ub] $ \c -> do
        pac  <- coeff p $ mkVecIdx $ xs ++          [c]
        qa0c <- coeff q $ mkVecIdx $ xs ++ vecΔ0 ++ [c]
        accumConstr [ mkEq (CAtom pac) (CAtom qa0c) ]

    -- r_j = q_{m+j}
    forM_ [1..k'] $ \j -> do
      rj  <- coeff r' $ RankIdx j
      qmj <- coeff q  $ RankIdx (m' + j)
      accumConstr [ mkEq (CAtom rj) (CAtom qmj) ]

    let _0 = replicate m 0

    if isTyTree τx
      then do
        -- p'_\ast = r_{k+1}
        rx <- coeff r' (RankIdx $ k' + 1)
        sx <- coeff s  astIdx
        accumConstr [ mkEq (CAtom rx) (CAtom sx) ]

        -- p'_{(a,c)} = r_{(\vec{0},a,c)}
        forM_ [0..ub] $ \a ->
          forM_ [0..ub] $ \c -> do
            p'ac <- coeff s  $ mkVecIdx $ a :          [c]
            r0ac <- coeff r' $ mkVecIdx $ a : vecΔ0 ++ [c]
            accumConstr [ mkEq (CAtom p'ac) (CAtom r0ac) ]

        costFree <- isCostFree
        if costFree
          then do
            setRuleName "let : t : cf"

            -- \forall \vec{b} \neq \vec{0} ( ... )
            forM_ [xs | xs <- IntList.enum ub k', any (/= 0) xs] $ \b_ -> do
              -- P^{(\vec{b})}
              pb_ <- copyCtx p

              -- P'^{(\vec{b})}
              p'b_ <- prove rule3 pb_ (e1, τx)

              forM_ [xs | xs <- IntList.enum ub m', any (/= 0) xs] $ \a_ -> do
                forM_ [0..ub] $ \c -> do
                  qabc <- coeff q $ mkVecIdx $ a_ ++ b_ ++ [c]

                  -- q(a_,b_,c) /= 0
                  let lhs = mkNe (CAtom qabc) (CInt 0)

                  -- p^{(\vec{b})}_{(\vec{a},c)} = q_{(\vec{a},\vec{b},c)}
                  pb_ac <- coeff pb_ $ mkVecIdx $ a_ ++ [c]
                  let rlhs = mkEq (CAtom qabc) (CAtom pb_ac)

                  -- ∃! p'^{(\vec{b})}_{(a,c)} \neq 0 (p'^{(\vec{b})}_{(a,c)} = 1 \land a \neq 0)
                  p'b_acs <- forM [1..ub] $ \a -> do
                    coeff p'b_ $ mkVecIdx [a, c]
                  temp <- forM p'b_acs $ \p'b_ac -> do
                    let lhs = mkNe (CAtom p'b_ac) (CInt 1)
                    let rhs = (flip map) (L.delete p'b_ac p'b_acs) $ \p'b_ac ->
                                mkEq (CAtom p'b_ac) (CInt 0)
                    return $ mkImp lhs (mkAnd rhs)

                  let rrhs = mkOr temp
                  accumConstr [ mkImp lhs (mkAnd [rlhs, rrhs]) ]

              forM_ [0..ub] $ \a ->
                forM_ [0..ub] $ \c -> do
                  rbac <- coeff r' $ mkVecIdx $ b_ ++ [a, c]
                  if a == 0
                    then do
                      q0bc <- coeff q $ mkVecIdx $ _0 ++ b_ ++ [c]
                      accumConstr [ mkEq (CAtom q0bc) (CAtom rbac) ]
                    else do
                      p'b_ac <- coeff p'b_ $ mkVecIdx [a, c]
                      accumConstr [ mkEq (CAtom p'b_ac) (CAtom rbac) ]

          else do
            setRuleName "let : t"

            -- q_{(\vec{0},\vec{b},c)} = r_{(\vec{b},0,c)}
            forM_ (IntList.enum ub k') $ \b ->
              forM_ [0..ub] $ \c -> do
                let qi  = VecIdx . Vector.fromList $ _0 ++ b ++    [c]
                let r'i = VecIdx . Vector.fromList $       b ++ [0, c]
                q0bc  <- coeff q  qi
                r'b0c <- coeff r' r'i
                accumConstr [ mkEq (CAtom q0bc) (CAtom r'b0c) ]
      else do
        setRuleName "let : x"

        -- q_{(\vec{0},\vec{b},c)} = r_{(\vec{b},c)}
        forM_ (IntList.enum ub k') $ \b ->
          forM_ [0..ub] $ \c -> do
            let qi  = VecIdx . Vector.fromList $ _0 ++ b ++ [c]
            let r'i = VecIdx . Vector.fromList $       b ++ [c]
            q0bc <- coeff q  qi
            r'bc <- coeff r' r'i
            accumConstr [ mkEq (CAtom q0bc) (CAtom r'bc) ]

    conclude q e t

letOrder
  :: Ctx    -- ^ context/annotation
  -> Typed  -- ^ expression e1, i.e. let x = e1 in e2
  -> [Text]
letOrder q e1 = filter (`elem` ts) $ S.toList vΓ ++ S.toList vΔ
  where
    var' = var . fromTyped

    vΓΔ = ctxVars q   -- all variables in context Q
    vΓ  = var' e1     -- variables in expression e1
    vΔ  = vΓΔ S.\\ vΓ -- remaining variables (i.e. variables in
                      -- expression e2 and variables that later will be
                      -- subject to weakening)

    ts = Ctx.trees q
