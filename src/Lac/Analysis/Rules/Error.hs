{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Error where

import           Data.Text.Ext             as T
import           Lac.Analysis.Rules.Common

ruleError :: Ctx -> (Typed, Type) -> Error -> Gen ProofTree
ruleError q tyExpr theError = do
  q' <- emptyCtx def
  return $
    ProofTree
      (q, tyExpr, q')
      (RuleName "error")
      []
      []
      (Just . T.show $ theError)
