{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Match where

import           Prelude                   hiding (pi)

import qualified Data.IntList              as IntList
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

import qualified Data.List.Ext             as L
import qualified Data.Vector               as V
import           Data.Word

ruleMatch
  :: Rule               -- ^ continuation
  -> Rule
  -> Ctx                -- ^ context/annotation
  -> Text               -- ^ scrutinee
  -> (Typed, Type)      -- ^ typed expression (nil-case)
  -> (Text, Text, Text) -- ^ deconstructed tree (node)
  -> (Typed, Type)      -- ^ typed expression (node-case)
  -> Gen ProofTree
ruleMatch ruleNil ruleNode q x (e1, τe1) (x1, x2, x3) (e2, τe2) =
  do
    setRuleName "match"

    assert (τe1 == τe2) "ruleMatch: e1 and e2 have distinct types"

    let u@(Bound ub) = def
    let m = Ctx.length q - 1
    let m' = fromIntegral m :: Word8

    case L.splitAt m . Ctx.trees $ q of
      (_, [y]) | x == y ->
        return ()
      _ ->
        assert False $ "ruleMatch: expected variable " <> x <> " to be in last position"

    (_, p) <- splitCtx' u q x
    (_, r) <- splitCtx' u q x
    r' <- augmentCtx u r [x1, x3]

    -- generate all \vec{a}
    let allVecA = IntList.enum ub m'

    -- r_{(\vec{a}, a, a, b)} = q_{(\vec{a}, a, b)}
    forM_ allVecA $ \as ->
      forM_ [0..ub] $ \a ->
        forM_ [0..ub] $ \b -> do
          let vr = mkVecIdx $ as ++ [a, a] ++ [b]
          let vq = mkVecIdx $ as ++ [a]    ++ [b]
          rv <- coeff r' vr
          qv <- coeff q vq
          accumConstr [ mkEq (CAtom rv) (CAtom qv) ]

    -- p_{(\vec{a}, c)} = \sum_{a+b=c} q_{(\vec{a}, a, b)}
    forM_ allVecA $ \as ->
      forM_ [0..ub] $ \c -> do
        let vp = mkVecIdx $ as ++ [c]
        pv <- coeff p vp
        qvs <- forM (IntList.split c) $ \(a, b) -> do
          let vq = mkVecIdx $ as ++ [a, b]
          coeff q vq
        accumConstr [ mkEq (CAtom pv) (CSum . map CAtom $ qvs) ]

    -- r_{m+1} = r_{m+2} = q_{m+1}
    rx1 <- coeff r' (RankIdx $ m' + 1)
    rx3 <- coeff r' (RankIdx $ m' + 2)
    qx  <- coeff q  (RankIdx $ m' + 1)
    accumConstr
      [ mkEq (CAtom rx1) (CAtom rx3)
      , mkEq (CAtom rx3) (CAtom qx)
      ]

    -- r_{(\vec{0}, 1, 0, 0)} = r_{(\vec{0}, 0, 1, 0)} = q_{m+1}
    let vec0 = replicate m 0
    let vr1 = VecIdx . V.fromList $ vec0 ++ [1, 0, 0]
    let vr2 = VecIdx . V.fromList $ vec0 ++ [0, 1, 0]
    r0100 <- coeff r' vr1
    r0010 <- coeff r' vr2
    qm1 <- coeff q $ RankIdx (m' + 1)
    accumConstr
      [ mkEq (CAtom r0100) (CAtom r0010)
      , mkEq (CAtom r0010) (CAtom qm1)
      ]

    -- q_i = r_i = p_i
    forM_ [1..m'] $ \i -> do
      let index = RankIdx i
      qi <- coeff q  index
      ri <- coeff r' index
      pi <- coeff p  index
      accumConstr [ mkEq (CAtom qi) (CAtom ri)
                  , mkEq (CAtom ri) (CAtom pi)
                  ]

    -- recursive calls
    q1' <- prove ruleNil  p  (e1, τe1)
    q2' <- prove ruleNode r' (e2, τe2)

    -- equate "return" contexts
    eqCtx q1' q2'
    -- TODO: introduce intermediate annotated context `q'`?

    -- TODO: fix expression
    conclude
      q
      (TyMatch
        (TyVar x, tyTree)
        (hole, tyHole)
        ((x1, x2, x3), (hole, tyHole)), τe1)
      q2'
