{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.App where

import           Data.Expr
import           Data.Expr.FromTyped       (fromTyped)
import           Data.Expr.Types           (unfoldApp)
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

import           Control.Monad.Ext         (ifM)
import           Data.List.NonEmpty        (NonEmpty (..))

ruleApp
  :: Ctx
  -> (Typed, Type)
  -> Gen ProofTree
ruleApp q1 (e, τ) =
  do
    setRuleName "app"

    case unfoldApp (fromTyped e) of
      Var f :| xs | all isVar xs -> do
        let TyApp (TyVar _, τf) _ = e

        (q, q') <- lookupFunctionAnnotation f

        go (f, q, q') xs
      _ ->
        throwError . AssertionFailed $ "ruleApp: ill-formed expression"
 where
  go (f, q,  q') xs = do
    let m = Ctx.length q

    let v = mkVecIdx $ replicate m 0 ++ [2]

    q1v <- coeff q1 v
    qv  <- coeff q  v

    cost <- ifM isCostFree (return 0) (return 1)

    accumConstr [ mkEq (CAtom q1v) (CSum [CAtom qv, CInt cost]) ]

    forM_ (filter ((/= v) . fst) $ coeffs q) $ \(i, qi) -> do
      q1i <- coeff q1 i
      accumConstr [ mkEq (CAtom qi) (CAtom q1i) ]

    conclude q1 (e, τ) q'
