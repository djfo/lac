{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Nil where

import qualified Data.IntList              as IntList
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

import qualified Data.List.Ext             as L
import qualified Data.Vector               as V

import Debug.Trace

ruleNil :: Ctx -> (Typed, Type) -> Gen ProofTree
ruleNil q (e, τ) =
  do
    setRuleName "nil"

    assert (Ctx.empty q) $ "ruleNil: context not empty"

    q' <- returnCtx def

    let Bound u = def

    -- q_{(c)} = \sum_{a+b=c} q'_{(a,b)}
    forM_ [0..u] $ \c -> do
      qc <- coeff q $ VecIdx . V.fromList $ [c]
      q'abs <- forM (IntList.split c) $ \(a, b) ->
        coeff q' $ VecIdx . V.fromList $ [a, b]
      accumConstr [ mkEq (CAtom qc) (CSum (map CAtom q'abs)) ]

    conclude q (nil, τ) q'
