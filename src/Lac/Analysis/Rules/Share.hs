{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Rules.Share where

import qualified Data.IntList              as IntList
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

import qualified Data.Vector               as Vector

ruleShare :: Rule -> Rule
ruleShare dispatch q tyExprS@(TyShare u (u1, u2) e, τ) = do
  setRuleName "share"
  splitCtx def q [u] >>= \case
    ([_], q') -> do
      -- TODO: assert `isTyTree τx`

      r <- augmentCtx def q' [u1, u2]

      -- equate unaffected coefficients
      forM_ (trees q') $ \t -> do
        qt <- varCoeff q t
        rt <- varCoeff r t
        accumConstr [ mkEq (CAtom qt) (CAtom rt) ]

      -- split potential for x
      qu  <- varCoeff q u
      qu1 <- varCoeff r u1
      qu2 <- varCoeff r u2
      accumConstr [ mkEq (CAtom qu) (CSum [CAtom qu1, CAtom qu2]) ]

      -- equate corresponding coefficients
      let Bound strictUpperBound = def
      forM_ (IntList.enum strictUpperBound (fromIntegral $ Ctx.length q')) $ \as ->
        forM_ [0..strictUpperBound] $ \b ->
          forM_ [0..strictUpperBound] $ \c ->
            forM_ (IntList.split c) $ \(c1, c2) -> do
              let qi = VecIdx . Vector.fromList $ as ++ [c,      b]
              let ri = VecIdx . Vector.fromList $ as ++ [c1, c2, b]
              qacb    <- coeff q qi
              rac1c2b <- coeff r ri
              let constraint = mkEq (CAtom qacb) (CAtom rac1c2b)
              accumConstr [constraint]

      q1 <- prove dispatch r (e, τ)

      conclude q tyExprS q1
    _ ->
      error "ruleShare: context split failed"

shareOrder :: Ctx -> Text -> [Text]
shareOrder _ u = [u]
