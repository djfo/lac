{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.WVar where

import qualified Data.IntList              as IntList
import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

import qualified Data.Vector               as V

ruleWVar
  :: Rule           -- ^ continuation
  -> Ctx            -- ^ context/annotation
  -> (Typed, Type)  -- ^ typed expression
  -> [Text]         -- ^ exceptions (variables that are kept)
  -> Gen ProofTree
ruleWVar dispatch q e xs =
  do
    setRuleName "w : var"

    let b@(Bound u) = def
    let m = Ctx.length q - 1 -- q = Γ,x:α|Q
    let m' = fromIntegral m

    (y, r) <- weakenCtx b q xs

    let mkRel = mkLe

    -- r_i = q_i
    forM_ (enumRankCoeffs r) $ \(idx, ri) -> do
      qi <- coeff q idx
      accumConstr [mkRel (CAtom qi) (CAtom ri)]

    -- r_{(\vec{a},b)} = q_{(\vec{a},0,b)}
    forM_ (IntList.enum u m') $ \as -> do
      forM_ [0..u] $ \b -> do
        let vab = VecIdx . V.fromList $ as ++ [b]
        let va0b = VecIdx . V.fromList $ as ++ [0, b]
        rab <- coeff r vab
        qa0b <- coeff q va0b
        accumConstr [mkRel (CAtom rab) (CAtom qa0b)]

    q' <- prove dispatch r e
    conclude q e q'
