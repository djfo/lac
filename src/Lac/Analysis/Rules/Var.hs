{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Lac.Analysis.Rules.Var where

import           Lac.Analysis.Rules.Common
import qualified Lac.Analysis.Types.Ctx    as Ctx

ruleVar :: Ctx -> (Typed, Type) -> Gen ProofTree
ruleVar q (TyVar x, τ) =
  do
    setRuleName "var"

    let u = def

    if isTyTree τ
      then do
        splitCtx' u q x

        assert
          (Ctx.length q == 1)
          "ruleVar: context must consist of exactly one tree"

        q' <- returnCtx u

        -- preserve potential
        q1 <- coeff q (RankIdx 1)
        q'x <- coeff q' astIdx
        accumConstr [ mkEq (CAtom q1) (CAtom q'x) ]

        forM_ (coeffs q) $ \case
          (index@(VecIdx v), qv) -> do
            q'v <- coeff q' index
            accumConstr [ mkEq (CAtom qv) (CAtom q'v) ]
          _ -> return ()

        conclude q (TyVar x, τ) q'
      else do
        assert
          (Ctx.length q == 0)
          "ruleVar: context must not contain trees"

        conclude q (TyVar x, τ) q
