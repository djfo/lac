{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac.Analysis.Rules (
    module E
  , dispatch
  , dispatchReport
  , step
  ) where

import           Data.Expr.FromTyped
import           Data.Expr.Typed
import           Data.Type                (isTyTree)
import           Lac.Analysis.ProofTree
import           Lac.Analysis.Rules.App   as E
import           Lac.Analysis.Rules.Bool  as E
import           Lac.Analysis.Rules.Cmp   as E
import           Lac.Analysis.Rules.Error as E
import           Lac.Analysis.Rules.Ite   as E
import           Lac.Analysis.Rules.Let   as E
import           Lac.Analysis.Rules.Match as E
import           Lac.Analysis.Rules.Nat   as E
import           Lac.Analysis.Rules.Nil   as E
import           Lac.Analysis.Rules.Node  as E
import           Lac.Analysis.Rules.Share as E
import           Lac.Analysis.Rules.Shift as E
import           Lac.Analysis.Rules.Var   as E
import           Lac.Analysis.Rules.W     as E
import           Lac.Analysis.Rules.WVar  as E
import           Lac.Analysis.Types
import           Lac.Analysis.Types.Ctx   as Ctx
import           Lac.PP.Pretty

import           Control.Monad.Except     (catchError)
import qualified Data.Text                as T
import qualified Data.Text.IO             as T

dispatchReport :: Rule
dispatchReport context typedExpression@(expression, τ) =
    dispatch context typedExpression `catchError` handler
  where
    handler :: Error -> Gen ProofTree
    handler error = do
      liftIO . T.putStrLn . T.unlines $
        [ ""
        , "Expression:\n"
        , "  " <> (pretty . fromTyped $ expression)
        , ""
        ]
      throwError error

dispatch :: Rule
dispatch = catch (step dispatch)

catch :: Rule -> Rule
catch f q@Ctx{..} tyExpr@(e, τ) = f q tyExpr `catchError` handler
  where
    handler error = do
      liftIO $ do
        print ctxTrees
        T.putStrLn . pretty . fromTyped $ e
        print error
      ruleError q tyExpr error

step :: Rule -> Rule
step cont q tyExpr@(e, τ) =
  case e of
    TyMatch (TyVar x, _) tyExpr1 ((x1, x2, x3), tyExpr2) ->
      let ruleMatch' q' _ = ruleMatch cont cont q' x tyExpr1 (x1, x2, x3) tyExpr2
      in
      ruleShift ruleMatch' (pushBack q [x]) q tyExpr
    TyLit (TyLNat _) ->
      if Ctx.length q > 0
        then ruleWVar cont q tyExpr []
        else ruleW ruleNat q tyExpr
    TyLit (TyLBool _) ->
      if Ctx.length q > 0
        then ruleWVar cont q tyExpr []
        else ruleW ruleBool q tyExpr
    TyLit TyLNil ->
      if Ctx.length q > 0
        then ruleWVar cont q tyExpr []
        else ruleW ruleNil q tyExpr
    TyLit (TyLNode (TyVar x1) (TyVar _) (TyVar x3)) -> do
      if Ctx.length q > 2
        then ruleWVar cont q tyExpr [x1, x3]
        else ruleW ruleNode q tyExpr
    TyVar x | isTyTree τ ->
      if Ctx.length q > 1
        then ruleWVar cont q tyExpr [x]
        else ruleW ruleVar q tyExpr
    TyVar x ->
      if Ctx.length q > 0
        then ruleWVar cont q tyExpr [x]
        else ruleW ruleVar q tyExpr
    TyCmp _ (TyVar x1, _) (TyVar x2, _) ->
      if Ctx.length q > 0
        then ruleWVar cont q tyExpr []
        else ruleW ruleCmp q tyExpr
    TyIte (TyVar x, _) tyExpr1 tyExpr2 ->
      ruleIte cont cont q x tyExpr1 tyExpr2
    TyLet _ (e1, _) _ ->
      ruleShift (ruleLet cont cont cont) (letOrder q e1) q tyExpr
    TyApp (TyVar f, _) _ -> do
      let fxs = filter (isTyTree . snd) . freeVariables $ tyExpr
      if Ctx.length q == Prelude.length fxs
        then ruleW ruleApp q tyExpr
        else ruleWVar cont q tyExpr (map fst fxs)
    TyShare u _ _ ->
      ruleShift (ruleShare dispatch) (shareOrder q u) q tyExpr
    _ ->
      throwError (AssertionFailed "_step: rule unimplemented")
