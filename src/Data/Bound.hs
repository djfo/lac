module Data.Bound (
    Bound(..)
  ) where

import           Data.Default
import           Data.IntList (enum)
import           Data.Word

-- | Note that the upper bound must be at least 2 to allow representation
-- of cost of function application.
newtype Bound
  = Bound {
    unBound :: Word8
  }
  deriving (Eq, Show)

instance Default Bound where
  def = Bound 2

-- | Enumerate assignments for logarithm coefficients.
enumA :: Integral a => a -> a -> a -> [[a]]
enumA u v n =
  concat [ [xs ++ [x]
           | x <- [0..v]
           ]
         | xs <- enum u n
         ]
