{-# LANGUAGE BangPatterns #-}

module Data.List.Ext (
    module E
  , elemElem
  , equal
  , for
  , subseq
  , count
  , dup
  , findWithIndex
  ) where

import Data.Nat

import           Data.List as E

equal :: Eq a => [a] -> Bool
equal [] = True
equal [_] = True
equal (x1:x2:xs) =
  if x1 == x2
    then equal (x2:xs)
    else False

for :: [a] -> (a -> b) -> [b]
for = flip map

elemElem :: Eq a => a -> [a] -> Bool
elemElem x ys =
  case break (== x) ys of
    (_, [])      -> False
    (_, (_:ys')) -> x `elem` ys'

subseq :: (a -> a -> Bool) -> [a] -> [[a]]
subseq p = go []
  where
    go acc    []     = [reverse acc]
    go []     (y:ys) = go [y] ys
    go (a:as) (y:ys)
        | p a y      =                  go (y:a:as) ys
        | otherwise  = reverse (a:as) : go [y]      ys

dup :: Eq a => a -> [a] -> Bool
dup x xs =
  case count x xs of
    S (S _) -> True
    _       -> False

count :: Eq a => a -> [a] -> Nat
count x [] = Z
count x (y:ys)
  | x == y    = S (count x ys)
  | otherwise = count x ys

findWithIndex :: (a -> Maybe b) -> [a] -> Maybe (Int, b)
findWithIndex f = go 0
  where
    go !acc (x:xs) | Just y <- f x = Just (acc, y)
                   | otherwise     = go (acc + 1) xs
    go _    []                     = Nothing
