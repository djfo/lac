module Data.Text.Ext (
    module T
  , show
  ) where

import           Data.Text as T
import           Prelude   hiding (show)
import qualified Prelude

show :: Show a => a -> Text
show = T.pack . Prelude.show
