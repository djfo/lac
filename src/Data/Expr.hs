module Data.Expr (
    module E
  , fromString
  ) where

import           Data.Expr.LetNF  as E
import           Data.Expr.Pretty as E
import           Data.Expr.Types  as E
import           Lac.Parser       as E (decl, expr, prog)
import qualified Lac.Parser       as Parser

import           Text.Parsec      as Parsec (parse)

fromString :: String -> Expr
fromString string =
  case parse Parser.expr "<interactive>" string of
    Left _           -> error "no parse"
    Right expression -> expression
