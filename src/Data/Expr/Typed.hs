{-# LANGUAGE GADTs              #-}
{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}

module Data.Expr.Typed (
    Typed(..)
  , TyLiteral(..)
  , TypedDecl(..)

  , Typed.freeVariables
  , substitute
  , rename

  , fromString

  , unshare
  ) where

import qualified Data.Expr               as Expr (fromString)
import           Data.Expr.FromTyped
import           Data.Expr.Typed.Typed   as Typed
import           Data.Expr.Typed.Unshare (unshare)
import           Data.Type

import           Data.Text               (Text)

data TypedDecl
  = TypedDecl {
    tyDeclId :: Text
  , tyDeclArgs :: [Text]
  , tyDeclExpr :: Typed
  , tyDeclType :: Type
  -- TODO: type signature (including annotation)
  }
  deriving Show

fromString :: String -> Typed
fromString = untyped . Expr.fromString
