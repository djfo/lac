{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE RecordWildCards #-}

module Data.Expr.LetNF (
    toLetNF
  , isLetNF
  ) where

import           Data.Expr.Types

import           Control.Monad.State.Strict.Ext
import           Data.List.NonEmpty             (NonEmpty (..))
import           Data.Text                      (Text)
import qualified Data.Text                      as T

-- * Let normal form

class ToLNF a where
  -- | Convert a value (e.g. an expression) to let-normal-form.
  toLetNF :: a -> a

instance ToLNF Expr where
  toLetNF = fst . flip runState 0 . letNF

instance ToLNF Decl where
  toLetNF decl@Decl{..} =
    decl { declExpr = toLetNF declExpr }

-- | Check if an expression is in let-normal-form.
isLetNF :: Expr -> Bool
isLetNF =
  \case
    Lit (LNat _)                        -> True
    Lit (LBool _)                       -> True
    Lit LNil                            -> True
    Lit (LNode (Var _) (Var _) (Var _)) -> True
    Var _                               -> True
    Cmp _ (Var _) (Var _)               -> True
    Ite (Var _) (Var _) (Var _)         -> True
    Let _ e1 e2                         -> isLetNF e1 && isLetNF e2
    App (Var _) e                       -> go e
      where
        go (Var _)         = True
        go (App (Var _) e) = go e
        go _               = False
    Match (Var _) e1 (xs, e2)           -> isLetNF e1 && isLetNF e2
    Abs _ (Var _)                       -> True
    _                                   -> False

letNF :: Monad m => Expr -> StateT Int m Expr
letNF expression
  | isLetNF expression = return expression
  | otherwise          = do (л, xs) <- lnf expression
                            return $ mkLet xs л

-- | Transform expression into let-normal-form, i.e. return a expression in
-- let-normal-form and a list of let-bindings.
lnf :: Monad m => Expr -> StateT Int m (Expr, [(Text, Expr)])
lnf expression =
  case expression of
    Var x -> return (Var x, [])

    Lit (LNode e1 e2 e3) ->
      do
        (л1, xs) <- vnf e1
        (л2, ys) <- vnf e2
        (л3, zs) <- vnf e3
        return (Lit (LNode (Var л1) (Var л2) (Var л3)), xs ++ ys ++ zs)
    Lit _ ->
      do
        л <- fresh'
        return (Var л, [(л, expression)])

    Cmp op e1 e2 ->
      do
        (л1, xs) <- vnf e1
        (л2, ys) <- vnf e2
        return (Cmp op (Var л1) (Var л2), xs ++ ys)

    Ite e1 e2 e3 ->
      do
        (л1, xs) <- vnf e1
        let e2' = toLetNF e2
        let e3' = toLetNF e3
        return (Ite (Var л1) e2' e3', xs)

    Let x e1 e2 ->
      do
        let e1' = toLetNF e1
        let e2' = toLetNF e2
        return (Let x e1' e2', [])

    App _ _ ->
      do
        let t :| ts = unfoldApp expression
        let split xs = (map fst xs, concatMap snd xs)
        (variables, bindings) <- split <$> mapM vnf (t : ts)
        let app = foldl1 App (map Var variables)
        return (app, bindings)

    Abs x e ->
      return (Abs x (toLetNF e), [])

    Match e1 e2 (ys, e3) -> do
      (л, xs) <- vnf e1
      return (Match (Var л) (toLetNF e2) (ys, toLetNF e3), xs)

-- | Transform an expression into variable-normal-form, i.e. return
-- a variable and a list of let-bindings.
vnf :: Monad m => Expr -> StateT Int m (Text, [(Text, Expr)])
vnf e = do
  (e', xs) <- lnf e
  case e' of
    Var л -> return (л, xs)
    _ -> do
      л <- fresh'
      return (л, xs ++ [(л, e')])

fresh' :: Monad m => StateT Int m Text
fresh' = decorate <$> fresh

decorate :: Int -> Text
decorate = T.pack . ("л" ++) . show

mkLet :: [(Text, Expr)] -> Expr -> Expr
mkLet = flip $ foldr (uncurry Let)
