module Data.Expr.Typed.Arbitrary where

import           Data.Expr

import           Control.Monad   (liftM, liftM2, liftM3)
import           Data.Text       (Text)
import qualified Data.Text       as T (pack)
import           Test.QuickCheck

class ToExpr a where
  toExpr :: a -> Expr

instance ToExpr LitTree where
  toExpr LitTreeNil             = Lit LNil
  toExpr (LitTreeNode e1 e2 e3) = Lit (LNode e1 e2 e3)

newtype Var
  = MkVar Text
  deriving (
    Eq
  , Ord
  , Show
  )

-- TODO: arbitrary length identifiers
instance Arbitrary Var where
  arbitrary = growingElements [MkVar (T.pack [c]) | c <- ['a'..'z']]

newtype LitBool
  = LitBool Bool
  deriving (
    Eq
  , Ord
  , Show
  )

instance Arbitrary LitBool where
  arbitrary = liftM LitBool arbitrary

newtype LitNat
  = LitNat Int
  deriving (
    Eq
  , Ord
  , Show
  )

instance Arbitrary LitNat where
  arbitrary = liftM (LitNat . abs) arbitrary

data LitTree
  = LitTreeNil
  | LitTreeNode Expr Expr Expr
  deriving (
    Eq
  , Ord
  , Show
  )

instance Arbitrary LitTree where
  arbitrary = sized arbTree
    where
      arbTree 0 = growingElements [LitTreeNil]
      arbTree 1 = liftM (flip (LitTreeNode (Lit LNil)) (Lit LNil)) arbitrary
      arbTree n =
        frequency $
          [ (1, return $ LitTreeNil)
          , (2, liftM3 LitTreeNode arbitrary arbitrary arbitrary)
          ]

instance Arbitrary Expr where
  arbitrary = sized arbExp
    where
      arbExp n =
          frequency $
            [ (2, arbitrary >>= \(LitNat i) -> return $ Lit (LNat i))
            ]
            ++
            concat
            [ [ (3, arbitrary >>= \(MkVar x) -> liftM (Abs x) arbExp1)
              , (3, liftM2 App arbExp1 arbExp2)
              ]
            | n > 0
            ]
        where
          arbExp1 = arbExp $ n - 1
          arbExp2 = arbExp $ n `div` 2

  shrink (Lit (LNode e1 e2 e3))
      = [ e1, e2, e3 ]
     ++ [ Lit (LNode e1' e2 e3) | e1' <- shrink e1 ]
     ++ [ Lit (LNode e1 e2' e3) | e2' <- shrink e2 ]
     ++ [ Lit (LNode e1 e2 e3') | e3' <- shrink e3 ]
  shrink (Abs x e)
      = [ e ]
     ++ [ Abs x e' | e' <- shrink e ]
  shrink (App e1 e2)
      = [ e1, e2 ]
--   ++ [ ab | Abs x e1' <- [e1]
--           , let ab = subst x e2 e1'
--           ]
     ++ [ App e1' e2  | e1' <- shrink e1 ]
     ++ [ App e1  e2' | e2' <- shrink e2 ]
