{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Expr.Typed.Pretty (ppTyped) where

import           Data.Expr.Pretty      (ppCmpOp)
import           Data.Expr.Typed.Typed
import           Data.Type

import           Data.Text             (Text)
import qualified Data.Text.Ext         as T

ppTyped :: (Typed, Type) -> Text
ppTyped (expression, ty) = go expression -- "(" <> go expr <> " : " <> ppType ty <> ")"
  where
    go =
      \case
        TyLit literal -> case literal of
          TyLNil           -> "nil"
          TyLNode e1 e2 e3 -> "(" <> ppTyped (e1, tyTree) <> ", " <> ppTyped (e2, tyNat) <> ", " <> ppTyped (e3, tyTree) <> ")"
          TyLBool True     -> "true"
          TyLBool False    -> "false"
          TyLNat n         -> T.show n

        TyVar x -> ppVar x

        TyCmp op e1 e2 ->
          ppTyped e1 <> " " <> ppCmpOp op <> " " <> ppTyped e2

        TyIte e1 e2 e3 ->
          "if " <> ppTyped e1 <> " then " <> ppTyped e2 <> " else " <> ppTyped e3

        TyLet x e1 e2 ->
          "let " <> ppVar x <> " = " <> ppTyped e1 <> " in " <> ppTyped e2

        TyApp e1 e2 ->
          "(" <> ppTyped e1 <> " " <> ppTyped e2 <> ")"

        TyMatch e1 e2 ((l, x, r), e3) ->
          "match " <> ppTyped e1 <> " with | nil -> " <> ppTyped e2 <> " | (" <> l <> ", " <> x <> ", " <> r <> ") -> " <> ppTyped e3

        TyAbs x e ->
          "\\ " <> x <> " -> " <> ppTyped e

        TyShare x (x1, x2) e ->
          "share " <> x <> " as (" <> x1 <> ", " <> x2 <> ") in " <> ppTyped (e, ty)

ppVar :: Text -> Text
ppVar = T.concatMap expand
  where
    expand = \case
      'л' -> "Λ"
      '0' -> "₀"
      '1' -> "₁"
      '2' -> "₂"
      '3' -> "₃"
      '4' -> "₄"
      '5' -> "₅"
      '6' -> "₆"
      '7' -> "₇"
      '8' -> "₈"
      '9' -> "₉"
      c   -> T.singleton c
