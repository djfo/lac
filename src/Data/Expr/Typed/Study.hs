{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}

module Data.Expr.Typed.Study where

import           Data.Expr.Typed.Typed
import           Data.Type

import           Control.Monad.Writer.Strict
import           Data.Text                   (Text)

data Ref
  = RVar Text
  | RNode Text Text Text
  | RNil
  deriving (Eq, Show)

data Rel
  = REq Ref Ref
  | RLt Ref Ref
  deriving (Eq, Show)

newtype KB a
  = KB {
    unKB :: Writer [Rel] a
  }
  deriving (
    Functor
  , Applicative
  , Monad
  , MonadWriter [Rel]
  )

-- TODO: check type
study :: (Typed, Type) -> KB ()
study (e, τ) = case e of
  TyLit literal -> case literal of
    TyLNode e1 e2 e3 -> do
      study (e1, tyTree)
      study (e2, tyNat)
      study (e3, tyTree)
    _ -> return ()
  TyVar _ -> return ()
  TyCmp _ e1 e2 -> do
    study e1
    study e2
  TyIte e1 e2 e3 -> do
    study e1
    study e2
    study e3
  TyLet x t1@(e1, τ1) t2@(e2, _) -> do
    when (isTyTree τ1) $
      case e1 of
        TyLit (TyLNode (TyVar l) (TyVar y) (TyVar r)) ->
          tell [ REq (RVar x) (RNode l y r) ]
        TyLit TyLNil -> tell [ REq (RVar x) RNil ]
        _            -> return ()
    study t1
    study t2
  TyApp t1 t2 -> do
    study t1
    study t2
  TyMatch (TyVar t, _) t1 ((l, _, r), t2) -> do
    tell [ RLt (RVar l) (RVar t)
         , RLt (RVar r) (RVar t)
         ]
    study t1
    study t2
  TyMatch _ _ _ -> return ()
  TyShare t (t1, t2) e -> do
    tell [ REq (RVar t) (RVar t1)
         , REq (RVar t) (RVar t2)
         ]
    -- TODO: fix
    study (e, tyHole)
  _ -> error "study: unexpected input"

execStudy :: KB a -> [Rel]
execStudy = snd . runWriter . unKB
