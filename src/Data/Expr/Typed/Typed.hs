{-# LANGUAGE GADTs              #-}
{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE StandaloneDeriving #-}

module Data.Expr.Typed.Typed (
    Typed(..)
  , TyLiteral(..)

  , isTyVar

  , freeVariables

  , substitute
  , rename
  ) where

import           Data.Expr.Types    hiding (var)
import           Data.Type

import           Control.Arrow      (first)
import           Data.Text          (Text)

data TyLiteral
  = TyLNil
  | TyLNode Typed Typed Typed
  | TyLBool Bool
  | TyLNat Int

deriving instance Show TyLiteral
deriving instance Eq TyLiteral
deriving instance Ord TyLiteral

data Typed where
  TyLit :: TyLiteral -> Typed
  TyVar :: Text -> Typed
  TyCmp :: CmpOp -> (Typed, Type) -> (Typed, Type) -> Typed
  TyIte :: (Typed, Type) -> (Typed, Type) -> (Typed, Type) -> Typed
  TyLet :: Text -> (Typed, Type) -> (Typed, Type) -> Typed
  TyApp :: (Typed, Type) -> (Typed, Type) -> Typed
  TyMatch :: (Typed, Type) -> (Typed, Type) -> ((Text, Text, Text), (Typed, Type)) -> Typed
  TyAbs :: Text -> (Typed, Type) -> Typed
  TyShare :: Text -> (Text, Text) -> Typed -> Typed

deriving instance Show Typed
deriving instance Eq Typed
deriving instance Ord Typed

isTyVar :: Typed -> Bool
isTyVar = \case
  TyVar _ -> True
  _       -> False

-- | Return free variables in typed expression.
freeVariables :: (Typed, Type) -> [(Text, Type)]
freeVariables (e, τ) =
  case e of
    TyLit literal ->
      case literal of
        TyLNil           -> []
        TyLNode e1 e2 e3 -> concatMap var $ [(e1, tyTree), (e2, tyNat), (e3, tyTree)]
        TyLBool _        -> []
        TyLNat _         -> []
    TyVar x -> [(x, τ)]
    TyCmp _ e1 e2 -> var e1 ++ var e2
    TyIte e1 e2 e3 -> var e1 ++ var e2 ++ var e3
    TyLet x e1 e2 -> var e1 ++ filter ((/= x) . fst) (var e2)
    TyApp e1 e2 -> var e1 ++ var e2
    TyMatch e1 e2 ((x1, x2, x3), e3) -> var e1 ++ var e2 ++ (var e3 `without` [x1, x2, x3])
    TyAbs x e -> filter ((/= x) . fst) (var e)
    TyShare _ _ _ -> error "freeVariables: unexpted `TyShare`"
  where
    xs `without` ys = filter ((`notElem` ys) . fst) xs
    var = freeVariables

-- | Substitute variable with term.
substitute :: Text -> Typed -> Typed -> Typed
substitute y t expression =
  case expression of
    TyLit (TyLNode t1 t2 t3) -> TyLit (TyLNode (rec' t1) (rec' t2) (rec' t3))
    TyVar x        | x == y  -> t
    TyCmp op e1 e2           -> TyCmp op (rec e1) (rec e2)
    TyIte e1 e2 e3           -> TyIte (rec e1) (rec e2) (rec e3)
    TyLet x e1 e2  | x /= y  -> TyLet x (rec e1) (rec e2)
    TyApp e1 e2              -> TyApp (rec e1) (rec e2)
    TyAbs x e      | x /= y  -> TyAbs x (rec e)
    TyMatch e1 e2 (xs, e3)   -> TyMatch (rec e1) (rec e2) (case_ xs e3)
    _                        -> expression
  where
    case_ xs@(x1, x2, x3) e
      | y `elem` [x1, x2, x3] = (xs, e)
      | otherwise             = (xs, rec e)

    rec  = first rec'
    rec' = substitute y t

-- | Rename all occurences of a variable in an expression.
rename :: Text -> Text -> Typed -> Typed
rename x x' = substitute x (TyVar x')
