{-# LANGUAGE OverloadedStrings #-}

module Data.Expr.Typed.Unshare (unshare) where

import           Data.Expr.Typed.Typed      as Typed
import           Data.Expr.Types
import           Data.Type

import           Control.Monad.State.Strict
import           Data.List.Ext              (dup)
import           Data.Set                   (Set)
import qualified Data.Set                   as S
import           Data.Text                  (Text)

-- | Unshare variables in expression, i.e. explicitly show shared variables
-- in syntax tree.
unshare :: Typed -> Typed
unshare = fst . flip runState 0 . go
  where
    go :: Typed -> State Int Typed
    go expression =
      case expression of
        TyLit literal -> case literal of
          TyLNode e1 e2 e3 | (x, _) : _ <- shared (e1, tyTree) (e3, tyTree) ->
            let x1 = x <> "_1"
                x2 = x <> "_2"
                e1' = rename x x1 e1
                e3' = rename x x2 e3
                share = TyShare x (x1, x2)
            in
              return $ share (TyLit (TyLNode e1' e2 e3'))
          _ -> return expression

        TyVar _ -> return expression

        TyCmp op typed1@(e1, τ1) typed2@(e2, τ2) ->
          case shared typed1 typed2 of
            [] -> do
              e1' <- go e1
              e2' <- go e2
              return $ TyCmp op (e1', τ1) (e2', τ2)
            (x, _) : _ ->
              let x1 = x <> "_1"
                  x2 = x <> "_2"
                  e1' = rename x x1 e1
                  e2' = rename x x2 e2
                  share = TyShare x (x1, x2)
              in
              go (TyCmp op (e1', τ1) (e2', τ2)) >>= return . share

        TyIte ty1@(e1, τ1) ty2@(e2, τ2) ty3@(e3, τ3) ->
          if null (shared ty1 ty2) && null (shared ty1 ty3) then
            case shared ty2 ty3 of
              [] -> do
                e2' <- go e2
                e3' <- go e3
                return $ TyIte ty1 (e2', τ2) (e3', τ3)
              (x, _) : _ ->
                let x1 = x <> "_1"
                    x2 = x <> "_2"
                    e2' = rename x x1 e2
                    e3' = rename x x2 e3
                    share = TyShare x (x1, x2)
                in
                  go (TyIte ty1 (e2', τ2) (e3', τ3)) >>= return . share
           else
            error "unshare: unsupported if (sharing between predicate and then/else-bodies)"

        TyLet y typed1@(e1, τ1) typed2@(e2, τ2) ->
          case shared typed1 typed2 of
            [] -> do
              e1' <- go e1
              e2' <- go e2
              return $ TyLet y (e1', τ1) (e2', τ2)
            (x, _) : _ ->
              let x1 = x <> "_1"
                  x2 = x <> "_2"
                  e1' = rename x x1 e1
                  e2' = rename x x2 e2
                  share = TyShare x (x1, x2)
              in
              go (TyLet y (e1', τ1) (e2', τ2)) >>= return . share

        TyApp typed1@(e1, τ1) typed2@(e2, τ2) ->
          case shared typed1 typed2 of
            [] -> do
              e1' <- go e1
              e2' <- go e2
              return $ TyApp (e1', τ1) (e2', τ2)
            (x, _) : _ ->
              let x1 = x <> "_1"
                  x2 = x <> "_2"
                  e1' = rename x x1 e1
                  e2' = rename x x2 e2
                  share = TyShare x (x1, x2)
              in
              go (TyApp (e1', τ1) (e2', τ2)) >>= return . share

        TyMatch typed1@(e1, τ1) typed2@(e2, τ2) (xs, typed3@(e3, τ3)) ->
          case (shared typed1 typed2, shared typed1 typed3, shared typed2 typed3) of
            (_:_, _, _) -> error "unshare: unsupported match (sharing between scrutinee and nil-case)"
            (_, _:_, _) -> error "unshare: unsupported match (sharing between scrutinee and node-case)"
            ([], [], []) -> do
              e2' <- go e2
              e3' <- go e3
              return $ TyMatch (e1, τ1) (e2', τ2) (xs, (e3', τ3))
            ([], [], (x, _) : _) ->
              let x1 = x <> "_1"
                  x2 = x <> "_2"
                  e2' = rename x x1 e2
                  e3' = rename x x2 e3
                  share = TyShare x (x1, x2)
              in
              go
                (TyMatch
                  (e1, τ1)
                  (e2', τ2)
                  (xs, (e3', τ3)))
                >>= return . share

        TyAbs x (e, τ) -> do
          e' <- go e
          return $ TyAbs x (e', τ)

        TyShare _ _ _ ->
          error "unshare: found `Share` node in input"

-- | List of trees shared between two expressions.
shared :: (Typed, Type) -> (Typed, Type) -> [(Text, Type)]
shared e1 e2 = S.toList $ shared' e1 e2

-- | Set of trees shared between two expressions.
shared' :: (Typed, Type) -> (Typed, Type) -> Set (Text, Type)
shared' e1 e2 =
  S.filter (isTyTree . snd) $
    S.fromList (Typed.freeVariables e1) `S.intersection` S.fromList (Typed.freeVariables e2)

-- | Test if set of shared variables (trees) of two expressions is disjunct.
disjunct :: (Typed, Type) -> (Typed, Type) -> Bool
disjunct e1 e2 = S.null $ shared' e1 e2

-- | Find non-linear variables out of a list of candidates.
nonLinear :: [Text] -> Expr -> [Text]
nonLinear [] _ = []
nonLinear (x:xs) expression =
  if dup x (var' expression)
    then x : nonLinear xs expression
    else     nonLinear xs expression
