module Data.Annotation.Annotation (
    Annotation(..)
  , Term(..)
  , LogTerm(..)
  , normalize
  , augment
  ) where

import           Control.Arrow (second)
import           Data.List     (partition, sortOn)
import           Data.Text     (Text)

newtype Annotation
  = Annotation [(Int, Term)]
  deriving (Eq, Ord, Show)

data Term
  = Rank Text
  | Log [(Int, LogTerm)]
  deriving (Eq, Ord, Show)

data LogTerm
  = Size Text
  | Cost
  deriving (Eq, Ord, Show)

-- | Augment argument to log(·) to include all tree variables.
-- Note: Input lists are assumed to be sorted.
augment :: [Text] -> [(Int, LogTerm)] -> [(Int, LogTerm)]
augment trees terms = go trees terms
  where
    go :: [Text] -> [(Int, LogTerm)] -> [(Int, LogTerm)]
    go [] []               = [(0, Cost)]
    go [] ((c, Cost) : []) = [(c, Cost)]
    go (x:xs) ts =
      case ts of
        (c, t) : ts' | Size y <- t, x == y -> (c, Size y) : go xs ts'
                     | otherwise           -> (0, Size x) : go xs ts
        []                                 -> map (\y -> (0, Size y)) (x:xs)

normalize' :: (Eq a, Num a, Eq b) => [(a, b)] -> [(a, b)]
normalize' [] = []
normalize' ((c, x) : xs) =
  let (matches, xs') = partition ((== x) . snd) xs
      c' = sum (c : map fst matches)
  in
  if c' /= 0
    then (c', x) : normalize' xs'
    else           normalize' xs'

-- | Combine coefficients and sort terms in annotation.
normalize :: Annotation -> Annotation
normalize (Annotation terms) =
    Annotation $ normalize' . sortOn snd . map (second f) $ terms
  where
    f (Rank x) = Rank x
    f (Log ts) = Log $ sortOn snd . normalize' $ ts
