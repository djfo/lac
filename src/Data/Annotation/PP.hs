{-# LANGUAGE OverloadedStrings #-}

module Data.Annotation.PP (
    ppAnnotation
  ) where

import           Data.Annotation.Annotation

import           Data.Text                  (Text)
import qualified Data.Text.Ext              as T

ppAnnotation :: Annotation -> Text
ppAnnotation (Annotation terms) =
  if null terms
    then "0"
    else T.intercalate " + " (map ppTerm terms)
  where
    ppTerm (c, Rank x) = T.show c <> "*rank(" <> x <> ")"
    ppTerm (c, Log ts) = T.show c <> "*log(" <> T.intercalate " + " (map ppLogTerm ts) <> ")"

    ppLogTerm (c, Cost) = T.show c
    ppLogTerm (c, Size x) = T.show c <> "*|" <> x <> "|"
