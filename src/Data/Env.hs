{-# LANGUAGE LambdaCase #-}

module Data.Env (
    Env
  , typingEnvironmentFromContext
  , inferContext
  ) where

import           Data.Expr
import           Data.Term
import           Data.Type

import           Control.Arrow (first)
import           Data.Char     (ord)
import           Data.List     (nub, sortOn)
import           Data.Text     (Text)
import qualified Data.Text     as T

type Env = [(T String Text, Type)]

typingEnvironmentFromContext :: [(Text, Type)] -> [(T String Text, Type)]
typingEnvironmentFromContext = map (first V)

-- Func: a, b, c, d, e, f, g, h
-- Nat: i, j, k, m, n, o
-- Bool: p, q, r, s
-- Tree: t, u, v, w, x, y, z
inferContext :: Expr -> [(Text, Type)]
inferContext = sortOn fst . nub . go
  where
    go =
      \case
        Lit (LNode e1 e2 e3) ->
          concat
            [ inferContext e1
            , inferContext e2
            , inferContext e3
            ]
        Var x -> [(x, typeOf x)]
        Cmp _ e1 e2 ->
          concat
            [ inferContext e1
            , inferContext e2
            ]
        Ite e1 e2 e3 ->
          concat
            [ inferContext e1
            , inferContext e2
            , inferContext e3
            ]
        Let x e1 e2 ->
          concat
            [ [(x, typeOf x)]
            , inferContext e1
            , inferContext e2
            ]
        App e1 e2 ->
          concat
            [ inferContext e1
            , inferContext e2
            ]
        Match e1 e2 (xs, e3) -> undefined
          error "inferContext: unimplemented (TODO)"
        Abs x e ->
          concat
            [ [(x, typeOf x)]
            , inferContext e
            ]
        _ -> []
    typeOf :: Text -> Type
    typeOf x =
      case T.unpack x of
        x1:_ | ord 'i' <= ord x1, ord x1 <= ord 'o' -> tyNat
        x1:_ | ord 'p' <= ord x1, ord x1 <= ord 's' -> tyBool
        x1:_ | ord 't' <= ord x1, ord x1 <= ord 'z' -> tyTree
        _ -> error $ "inferContext.typeOf: x == " <> show x
