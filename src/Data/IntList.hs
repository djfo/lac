module Data.IntList where

-- | Enumerate all lists of integers of given length, where the values
-- range between zero and the given strict upper bound.
enum :: Integral a => a -> a -> [[a]]
enum u = go
  where
    go 0 = [[]]
    go 1 = map (\x -> [x]) range
    go n = [i : xs | i <- range, xs <- go (n - 1)]

    range = [0..u]

split :: Integral a => a -> [(a, a)]
split x = [(a, x - a) | a <- [0..x]]

splits :: Integral a => Int -> a -> [[a]]
splits 0 _ = []
splits 1 x = [[x]]
splits 2 x = [[a, b] | (a, b) <- split x]
splits n x = concat $ [map (a:) (splits (n - 1) (x - a)) | a <- [0..x]]

predecessors :: [Int] -> [[Int]]
predecessors xs =
  case xs of
    _ | all (== 0) xs -> []
    [x]               -> [[x - 1]]
    x:xs              -> let hd | x > 0     = [x - 1 : xs]
                                | otherwise = []
                         in
                           hd <> [x : xs' | xs' <- predecessors xs]
