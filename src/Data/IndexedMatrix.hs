{-# LANGUAGE RecordWildCards   #-}

module Data.IndexedMatrix where

import qualified Data.List  as List
import           Data.Maybe (fromMaybe)
import qualified Data.Set   as Set
import           Data.Text  (Text)
import qualified Data.Text  as Text

data IM k
  = IM {
    imRows :: [[(k, Int)]]
  }
  deriving Show

getColumnByKey :: Eq k => k -> IM k -> [Int]
getColumnByKey key m@IM{..} =
  map (fromMaybe 0 . lookup key) imRows

getRows :: Ord k => IM k -> [[Int]]
getRows IM{..} =
    map (\row -> map (fromMaybe 0 . flip lookup row) keys) imRows
  where
    keys = Set.toList keys'
    keys' = foldr
             (\row set -> set `Set.union` (Set.fromList . map fst) row)
             mempty
             imRows
