{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Lac (
    parseProg
  , readProg
  , parseExpr
  , toLetNF
  , module E
  , Decl(..)
  , ppExpr
  , inferProgType
  , TypedDecl(..)
  , ppTyped
  , unshare
  , proof
  , proofWith
  , costFree
  , setFunctionAnnotation
  , gcMkDeclAnn
  , tyNat
  , tyTree
  , runGen
  , writeProofTrees
  , displayProofTrees
  , checkProofs
  , emptyCtx
  , augmentCtx
  , forceCoeff
  ) where

import           Data.Expr
import           Data.Expr.Typed
import           Data.Expr.Typed.Pretty
import           Data.Term
import           Data.Type
import           Data.TypeAnn
import           Lac.Analysis.ProofTree
import           Lac.Analysis.Rules
import           Lac.Analysis.Solver
import           Lac.Analysis.Types
import           Lac.Eval
import           Lac.Prog               as E
import           Lac.TypeInference

import           Data.List              (find)
import qualified Data.Map               as M
import           Data.Text              (Text)
import qualified Data.Text.IO           as T
import           Text.Parsec            (ParseError, parse)

parseProg :: Text -> Either ParseError Prog
parseProg text =
  case parse prog "<source>" text of
    Left e -> Left e
    Right (decls, tySigs) ->
      let env = M.fromList . map (\(Decl x xs e _) -> (x, Lac.Eval.fromDecl xs e)) $ decls
      in
        Right (Prog decls' env)
      where
        decls' = map (\(Decl x xs e _) -> Decl x xs e (find (\TypeSig{..} -> tsName == x) tySigs)) decls

readProg :: FilePath -> IO (Either ParseError Prog)
readProg path = do
  text <- T.readFile path
  return $ parseProg text

parseExpr :: Text -> Either ParseError Expr
parseExpr = parse expr "<source>"

proof :: TypedDecl -> Gen (Text, ProofTree)
proof = proofWith dispatch

proofWith :: Rule -> TypedDecl -> Gen (Text, ProofTree)
proofWith rule TypedDecl{..} = do
  (xs, e) <- either (throwError . AssertionFailed) return (unwrap tyDeclType tyDeclExpr)
  (q, _) <- lookupFunctionAnnotation tyDeclId
  let arguments = (q, (e, returnType xs tyDeclType))
  proofTree <- uncurry rule arguments
  reset
  return (tyDeclId, proofTree)

-- TODO: better error type
unwrap :: Type -> Typed -> Either Text ([(Text, Type)], Typed)
unwrap = go []
  where
    go :: [(Text, Type)] -> Type -> Typed -> Either Text ([(Text, Type)], Typed)
    go acc (F "->" [τx, τe]) (TyAbs x (e, _)) =
      let acc' = (x, τx) : acc
      in
      go acc' τe e
    go acc (F "->" _) e           = Left "unwrap: expression does not match type (1)"
    go acc _          (TyAbs _ _) = Left "unwrap: expression does not match type (2)"
    go acc _ e =
      Right (reverse acc, e)

returnType :: [a] -> Type -> Type
returnType []     τ                = τ
returnType (x:xs) (F "->" [_, τ2]) = returnType xs τ2
returnType _      _                = error "returnType: illegal arguments"
