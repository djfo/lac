{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           AnnotationParser              (annotation)
import           AppState                      (AppState)
import qualified AppState                      as AppState
import           Data.Annotation               as Annotation
import           Data.Env
import           Data.Expr
import           Data.Type
import           Lac.Analysis.ProofTree
import           Lac.Analysis.Rules
import           Lac.Analysis.Rules.Halt
import           Lac.Analysis.Types
import           Lac.Analysis.Types.Coeff
import           Lac.Analysis.Types.Constraint as Constraint
import           Lac.Analysis.Types.Ctx.Ctx    (lookupIndex)
import           Lac.Analysis.Types.PP
import           Lac.TypeInference
import           System.Repl

import           Control.Monad                 (void)
import           Control.Monad.State.Strict
import           Data.Char                     (isSpace)
import           Data.Default
import           Data.List.Ext                 (findIndex, findWithIndex,
                                                isPrefixOf)
import           Data.Text                     (Text)
import qualified Data.Text.Ext                 as T
import qualified Data.Text.IO                  as T
import qualified Data.Vector                   as V
import           System.Environment            (getArgs)
import           Text.Parsec

main :: IO ()
main =
  do
    args <- getArgs
    if null args
      then void $ repl "step> " AppState.empty handler
      else mapM_ (script handler AppState.empty) args

handler :: String -> StateT AppState IO Bool
handler line | ":quit" `isPrefixOf` line = do
  liftIO $ putStrLn "fin."
  return False
handler line = handleLine line >> return True

handleLine :: String -> StateT AppState IO ()
handleLine line =
  case break isSpace line of
    (":ann", _:arg) ->
      do
        let result = parse (annotation <* eof) "<interactive>" arg
        case result of
          Left theError -> liftIO $ print theError
          Right annotation -> do
            modify $ \s -> s { AppState.annotation = Just (Annotation.normalize annotation) }
    (":state", _) -> get >>= \s ->
      liftIO $ do
        print $ AppState.context s
        mapM_ (T.putStrLn . ppAnnotation) (AppState.annotation s)
    (":display", _) ->
      gets AppState.proofTree >>=
        \case
          Just proofTree -> liftIO $ displayProofTree proofTree
          Nothing -> liftIO $ putStrLn "no proof tree set"
    (":check", _) ->
      gets AppState.proofTree >>= \case
        Just proofTree -> liftIO $ do
          -- TODO: check proof
          error "unimplemented"
        Nothing ->
          liftIO $ putStrLn "no proof tree set"
    _ ->
      do
        let result = parse expr "(interactive)" line
        case result of
          Left theError -> liftIO $ print theError
          Right expression -> do
            let context = inferContext expression

            modify $ \s -> s { AppState.context = context }

            maybeAnnotation <- gets AppState.annotation

            (result, output) <- liftIO $ do
              print expression

              runGen def mempty $ do
                q0 <- emptyCtx def
                q  <- augmentCtx def q0 context

                forM_ maybeAnnotation $ \annotation -> do
                  annotation' <- f context annotation
                  apply q context annotation'

                let environment = typingEnvironmentFromContext context
                let (typed, τ) = inferType environment expression

                step (ruleHalt True) q (typed, τ)

            case result of
              Right proofTree@ProofTree{..} ->
                do
                  modify $ \s -> s { AppState.proofTree = Just proofTree }
                  let constraints = map (Constraint.mapCoeff σ) ptConstraints
                  liftIO $ mapM_ (T.putStrLn . ppConstr id) constraints
                where
                  (q, _, q') = ptConclusion

                  σ :: Coeff -> Text
                  σ coefficient =
                    let contexts = collectAnnotatedContexts proofTree
                    in
                    case findWithIndex (lookupIndex coefficient) contexts of
                      Just (contextIndex, coefficientIndex) ->
                        "Q" <> T.show contextIndex <> "_{" <> coefficientString <> "}"
                        where
                          coefficientString :: Text
                          coefficientString =
                            case coefficientIndex of
                              RankIdx x -> T.show x
                              VecIdx v -> "(" <> (T.intercalate "," . map T.show . V.toList $ v) <> ")"
                      Nothing -> "?"

              Left theError ->
                liftIO $ print theError

f :: [(Text, Type)] -> Annotation -> Gen Annotation
f context annotation@(Annotation terms) = do
  let terms' = map (\case t@(_, Rank _) -> t
                          (c, Log xs)   -> let xs' = augment (map fst context) xs
                                           in (c, Log xs')) terms

  return $ Annotation terms'

-- | Add constraints that are a consequence of the given context and annotation.
apply :: Ctx -> [(Text, Type)] -> Annotation -> Gen ()
apply q context (Annotation terms) =
    mapM_ makeConstraint terms
  where
    makeConstraint :: (Int, Term) -> Gen ()
    makeConstraint (c, Rank x) = do
      let index = RankIdx (succ . fromIntegral . position $ x)
      qx <- coeff q index
      accumConstr [mkEq (CInt c) (CAtom qx)]
    makeConstraint (c, Log ts) = do
      let index = VecIdx . V.fromList . map (fromIntegral . fst) $ ts
      qv <- coeff q index
      accumConstr [mkEq (CInt c) (CAtom qv)]

    position x =
      case findIndex ((==) x . fst) context of
        Just index -> index
        Nothing    -> error . T.unpack $ "convert: variable `" <> x <> "` not found in context"
