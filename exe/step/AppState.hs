module AppState where

import           Data.Annotation
import           Data.Type              (Type)
import           Lac.Analysis.ProofTree

import           Data.Text              (Text)

data AppState
  = AppState {
    context    :: [(Text, Type)]
  , annotation :: Maybe Annotation
  , proofTree  :: Maybe ProofTree
  }
  deriving (Eq, Ord, Show)

appState :: [(Text, Type)] -> Maybe Annotation -> Maybe ProofTree -> AppState
appState = AppState

empty :: AppState
empty = AppState mempty Nothing Nothing
