{-# LANGUAGE FlexibleContexts #-}

module AnnotationParser (
    annotation
  ) where

import           Prelude         hiding (log, sum)

import           Data.Annotation
import           Lac.Parser      (identifier)

import           Control.Monad   (void)
import           Data.Maybe      (fromMaybe)
import qualified Data.Text       as T
import           Text.Parsec

-- Spelled-out annotation
-- Example input: rank(x)+log(|x|+|x|+3)+rank(x)
annotation :: Stream s m Char => ParsecT s u m Annotation
annotation = Annotation <$> sum term

sum :: Stream s m Char => ParsecT s u m a -> ParsecT s u m [a]
sum = flip sepBy (keyword "+")

term :: Stream s m Char => ParsecT s u m (Int, Term)
term =
  do
    c <- fromMaybe 1 <$> optionMaybe (coefficient <* keyword "*")
    func <- rank <|> log
    return (c, func)

coefficient :: Stream s m Char => ParsecT s u m Int
coefficient = read <$> (many1 digit <* many (char ' '))

log :: Stream s m Char => ParsecT s u m Term
log =
  do
    void $ keyword "log"
    void $ keyword "("
    ts <- sum $ try _size <|> ((,) <$> (nat <* many (char ' ')) <*> pure Cost)
    void $ keyword ")"
    return $ Log ts
  where
    _size = do
      c <- fromMaybe 1 <$> optionMaybe (coefficient <* keyword "*")
      s <- size
      return (c, s)
    _1 x = (1, x)

rank :: Stream s m Char => ParsecT s u m Term
rank =
  do
    void $ keyword "rank"
    void $ keyword "("
    i <- T.pack <$> identifier
    void $ keyword ")"
    return $ Rank i

keyword :: Stream s m Char => String -> ParsecT s u m String
keyword word = string word <* many (char ' ')

size :: Stream s m Char => ParsecT s u m LogTerm
size =
  do
    void $ keyword "|"
    i <- T.pack <$> identifier
    void $ keyword "|"
    return $ Size i

nat :: Stream s m Char => ParsecT s u m Int
nat = read <$> many1 digit
