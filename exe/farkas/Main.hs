{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           Lac.Analysis.Types
import           Lac.Analysis.Types.Constraint
import           Lac.Analysis.Types.Ctx

import           Control.Monad                 (foldM, forM, forM_, when)
import           Data.Default
import           Data.IndexedMatrix
import qualified Data.IntList                  as IntList
import qualified Data.List.Ext                 as List
import           Data.Map.Strict               (Map)
import qualified Data.Map.Strict               as Map
import           Data.Maybe                    (catMaybes, fromJust)
import           Data.SBV
import           Data.SBV.Internals
import           Data.Text                     (Text)
import qualified Data.Text.Ext                 as Text
import           Data.Type

main :: IO ()
main = do
  -- result <- optimize Lexicographic problem
  -- print result

  (Right (q2, q3), _) <- runGen def mempty $ do
    let variables = [ ("cr", tyTree)
                    , ("bl", tyTree)
                    , ("br", tyTree)
                    ]
    q2 <- flip (augmentCtx def) variables =<< emptyCtx def
    q3 <- copyCtx q2
    return (q2, q3)

  constraints <- q3 `lessThanOrEqualTo` q2
  -- result <- optimize Lexicographic (problem constraints)
  let provable = problem constraints
  satResult@(SatResult result) <- sat provable

  case result of
    Unsatisfiable _ _ -> putStrLn "unsat"
    Satisfiable _ _   -> putStrLn "sat"
    SatExtField _ _   -> putStrLn "satextfield"
    Unknown _ _       -> putStrLn "unknown"
    ProofError _ _ _  -> putStrLn "error"

  --print satResult

  let model = getModelDictionary satResult

  forM_ (Map.toList model) $ \(key, value) ->
    when ("q_" `List.isPrefixOf` key) $ do
      let correspondingKey = 'r' : drop 1 key
      let Just correspondingValue = Map.lookup correspondingKey model
      case (cvVal value, cvVal correspondingValue) of
        (CFloat 0, CFloat 0) -> return ()
        _                        -> print (drop 2 key, value, correspondingValue)

  -- script <- generateSMTBenchmark True provable
  -- writeFile "script.smt" script

  return ()

makeAssertion :: Map Text SReal -> Constraint Text -> SBool
makeAssertion mapping constraint@(CRel op lhs rhs) =
    f (ast lhs) (ast rhs)
  where
    ast (CAtom key) = fromJust . Map.lookup key $ mapping
    ast (CInt i)    = fromIntegral i
    ast (CSum es)   = foldr (+) 0 (map ast es)
    ast (CProd es)  = foldr (*) 1 (map ast es)

    f = case op of
      CEq -> (.==)
      CLe -> (.<=)
      CGe -> (.>=)

problem constraints = do
  mapping <- foldM
    (\m k -> do
      case Map.lookup k m of
        Just v  -> return m
        Nothing -> do
          v <- sReal (Text.unpack k)
          return $ Map.insert k v m)
    mempty
    (concatMap atoms constraints)

  -- require coefficients to be non-negative integers
  forM_ (Map.toList mapping) $ \(key, x) ->
    when ("q_" `Text.isPrefixOf` key || "r_" `Text.isPrefixOf` key) $
      constrain $ 0 .<= x

  forM_ constraints $ \constraint -> do
    constrain $ makeAssertion mapping constraint

  -- maximize "goal" $ 5*x1 + 6*x2

  let force key value = do
        let Just x = Map.lookup key mapping
        constrain $ x .== value

  force "q_{X_{0,0,0,2}}" 2
  force "q_{X_{0,0,1,0}}" 1
  force "q_{X_{0,1,0,0}}" 1
  force "q_{X_{1,0,0,0}}" 1
  force "q_{X_{1,0,1,0}}" 1
  force "q_{X_{1,1,1,0}}" 1

  force "r_{X_{0,0,0,2}}" 1
  force "r_{X_{0,0,1,0}}" 1
  force "r_{X_{0,1,0,0}}" 3
  force "r_{X_{0,1,1,0}}" 1
  force "r_{X_{1,0,0,0}}" 1
  force "r_{X_{1,0,1,0}}" 1
  force "r_{X_{1,1,1,0}}" 1

  return ()

lessThanOrEqualTo q r =
  do
    let maxAssignment = List.replicate (List.length variables) strictUpperBound
    -- let maxAssignment = List.replicate (List.length variables - 1) 1 ++ [2]

    -- Ax ≤ b = 0
    let columns = map mkRef . IntList.enum strictUpperBound $ List.length variables
    let rows = makeLogConstraints maxAssignment
    let _A = IM rows

    -- f
    let f = ["f_{" <> Text.show i <> "}" | i <- [0 .. List.length rows - 1]]

    -- q ≤ fA + r
    forM columns $ \key -> do
      -- fA
      let sum = List.for (zip f (getColumnByKey key _A)) $ \(xf, xA) ->
            if xA /= 0
              then Just $ CProd [CInt xA, CAtom xf]
              else Nothing
      let sum' = CSum . catMaybes $ sum

      let qi = "q_{" <> key <> "}"
      let ri = "r_{" <> key <> "}"

      return $ CRel CLe (CSum [CAtom qi, CProd [CInt (-1), CAtom ri]]) sum'
  where
    strictUpperBound = 2
    sortedTrees = List.sort (trees q)
    variables = sortedTrees ++ ["$"]

-- | Make constraints on logarithmic terms that may appear in weakening
-- inequality.
makeLogConstraints :: [Int] -> [[(Text, Int)]]
makeLogConstraints assignment =
  if all (== 0) assignment
    then
      [[(mkRef assignment, 1)], [(mkRef assignment, -1)]]
    else
      concatMap
        (\predecessor ->
          [(mkRef assignment, -1), (mkRef predecessor, 1)]
          : makeLogConstraints predecessor)
        (IntList.predecessors assignment)

mkRef :: [Int] -> Text
mkRef xs =
    -- TODO: add fresh id
    "X_{" <> subscript <> "}"
  where
    subscript = Text.intercalate "," (map Text.show xs)
