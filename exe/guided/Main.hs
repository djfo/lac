{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           Data.Type               (tyNat)
import qualified Lac
import qualified Lac.Analysis.Rules      as Rules
import qualified Lac.Analysis.Rules.Halt as Rules
import qualified Lac.Analysis.Types.Ctx  as Ctx
import           Lac.Trace

import           Control.Monad           (foldM, forM, forM_, when)
import           Data.Default
import qualified Data.Map.Strict         as Map
import           Data.Text               (Text)
import qualified Data.Text.IO            as Text.IO

main :: IO ()
main = do
  text <- Text.IO.readFile "examples/splay.ml"
  let example_splay = make_example_splay text

  -- let example = example_nil
  -- let example = example_node
  -- let example = example_nested_node
  -- let example = example_inf
  -- let example = example_nested_node
  -- let example = example_cost_1
  -- let example = example_let
  let example = example_splay

  let showProof = False

  let (programText, variables, ruleTree) = example

  case Lac.parseProg programText of
    Left theError -> print theError
    Right prog@(Lac.Prog decls _) -> do
      let prog_lnf@(Lac.Prog lnfDecls _) = prog { Lac.progDecls = map Lac.toLetNF decls }
      -- let-normal-form
      putStrLn "LET-NORMAL-FORM"
      forM_ lnfDecls $ \(Lac.Decl identifier arguments expression typeSignature) ->
         Text.IO.putStrLn (Lac.ppExpr expression)
      -- typed
      putStrLn "TYPED"
      let typedDeclarations = Lac.inferProgType prog_lnf
      forM_ typedDeclarations $ \(Lac.TypedDecl identifier arguments tyExpr ty) -> do
        Text.IO.putStrLn $ Lac.ppTyped (tyExpr, ty)
        putStrLn "TYPE"
        print ty
      -- unshared
      putStrLn "UNSHARED"
      let unsharedTyDeclarations = map (\tyDecl@(Lac.TypedDecl _ _ e _) -> tyDecl { Lac.tyDeclExpr = Lac.unshare e }) typedDeclarations
      forM_ unsharedTyDeclarations $ \(Lac.TypedDecl identifier arguments tyExpr ty) -> do
        Text.IO.putStrLn $ Lac.ppTyped (tyExpr, ty)
      -- proof
      let config =
            def { Lac.gcMkDeclAnn = \Lac.TypedDecl{..} -> do
                    q <- flip (Lac.augmentCtx def) variables =<< Lac.emptyCtx def
                    r <- flip (Lac.augmentCtx def) ["*"] =<< Lac.emptyCtx def

                    let forceZeroCoefficients annotatedContext@Ctx.Ctx{..} = do
                          let indices = map fst . Map.toList $ ctxCoefficients
                          foldM
                            (\p index -> Lac.forceCoeff index 0 p)
                            annotatedContext
                            indices

                    -- q' <- forceZeroCoefficients q
                    let q' = q
                    -- r' <- forceZeroCoefficients r
                    let r' = r

                    Lac.setFunctionAnnotation tyDeclId (q', r')
                }
      (result, output) <- Lac.runGen config unsharedTyDeclarations $
        forM unsharedTyDeclarations $ \unTyDecl -> do
          Lac.proofWith ruleTree unTyDecl
      case result of
        Left theError -> print theError
        Right proofs -> do
          let proofTrees = map snd proofs
          when showProof $
            Lac.displayProofTrees proofTrees
          Lac.checkProofs proofs

-- nil

example_nil = ("f x = nil;", [], Rules.dispatch)

-- (nil, x, nil)

-- \ x -> let Λ₁ = nil in let Λ₂ = nil in (Λ₁, x, Λ₂)
example_node = ("f x = (nil, x, nil);", [], Rules.dispatch)

-- t' = (al, a', (ar, b, (br, c, cr)))

example_nested_node = (programText_nested_node, variables_nested_node, Rules.dispatch)
  where
    programText_nested_node = "f br cr ar al a' b c = (al, a', (ar, b, (br, c, cr)));"
    variables_nested_node = ["br", "cr", "ar", "al"]
    ruleTree_nested_node =
        Lac.costFree (Rules.ruleLet Rules.dispatch haltT haltT)
      where
        haltT = Rules.ruleHalt True

-- f x = f x

example_inf = ("f :: Tree Nat -> Tree Nat; f x = f x;", ["x"], Rules.dispatch)

-- f x = let y = x in f y

example_inf_let = ("f :: Tree Nat -> Tree Nat; f x = let y = x in f y;", ["x"], Rules.dispatch)

-- f x = x; g x = f x

example_cost_1 = ("f x = x; g x = f x;", [], Rules.dispatch)

-- f y = let x = nil in (x, y, x)

example_let = ("f y = let x = nil in (x, y, x);", [], Rules.dispatch)

make_example_splay text = (text, variables, Rules.dispatch)
  where
    variables = []
