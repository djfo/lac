# lac

## Usage

Load program and enter REPL

    $ lac -i <path/to/program.ml>

Show declarations of program

    > :decls
    delete a t = ...
    insert a t = ...
    splay a t = ...
    splay_max t = ...

Evaluate expression

    > insert 42 nil
    (nil, 42, nil)

Quit

    > :quit

Analyze program

    $ lac <path/to/program.ml>

Example run

    $ lac -i examples/splay.ml
    > insert 42 (insert 0 (insert 2 nil))
    (((nil, 0, nil), 2, nil), 42, nil)
    > delete 0 (insert 42 (insert 0 (insert 2 nil)))
    (nil, 2, (nil, 42, nil))
    > :quit

## Compiling and running `lac`

Prerequisites

- z3

Run the `lac` tool using [Stack](https://haskellstack.org/)

Interpret using `ghci`

    $ stack ghci

Compile using `ghc`

    $ stack build

Install into `$HOME/.local/bin`

    $ stack install

## OS-specific notes

### Debian

1. Download and unpack Ubuntu binary release of [z3](https://github.com/Z3Prover/z3/releases)
2. Build/run with extra library/include paths

    stack ghci \
        --extra-lib-dirs=$HOME/opt/z3/bin \
        --extra-include-dirs=$HOME/opt/z3/include

### macOS

Prerequisites

    $ brew install haskell-stack
    $ brew install z3
